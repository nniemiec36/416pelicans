# The Polsby-Popper (PP) measure (polsby & Popper, 1991) is the ratio of the area of the district (AD) 
# to the area of a circle whose circumference is equal to the perimeter of the district (PD). A district’s
#  Polsby-Popper score falls with the range of [0,1] and a score closer to 1 indicates a more compact district.

import json as json
import math as math
# import shapely as shapely
from shapely.geometry import Polygon, mapping, shape

cd_path = './GA_CD_ALL_INFO.json'
cd_file = open(cd_path, "r")
cd_data = json.load(cd_file)

popperScores = []
for i in range(len(cd_data["features"])):
    p = shape(json.loads(json.dumps(cd_data['features'][i]['geometry'])))
    # print(p.area)
    # print(p.length)
    area = p.area
    perimeter = p.length
    polsby = (4 * math.pi * area) / (perimeter**2)
    print(cd_data['features'][i]['properties']["DistrictID"] +" polsby "+str(polsby))
    popperScores.append(polsby)


planPopperScore = 0
for i in range(len(popperScores)):
    planPopperScore+=popperScores[i]
planPopperScore = planPopperScore / len(popperScores)
print(planPopperScore)
