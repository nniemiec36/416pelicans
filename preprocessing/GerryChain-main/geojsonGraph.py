from gerrychain import Graph
import geopandas as gpd
import sys

# first argument will be the path of the json file we are trying to convert 
# (precincts or census blocks geojson)
# second argument will be the abbrev of the state we are dealing with
# ie. ga, pa, nc
# third argument will be C or P for census or precinct

if len(sys.argv) != 4:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

geojson_path = sys.argv[1]
print(geojson_path)
if sys.argv[3] == 'C' or sys.argv[3] == 'c':
    graph_path = sys.argv[2] + '-census-blocks-graph.json'
elif sys.argv[3] == 'P' or sys.argv[3] == 'p':
    graph_path = sys.argv[2] + '-precincts-graph.json'
else:
    print("arguments not supported")
    exit(0)

print("file will be saved to: " + graph_path) 

df = gpd.read_file(geojson_path)
df.to_crs(inplace=True, crs="epsg:26918")

dual_graph = Graph.from_geodataframe(df, ignore_errors=True)

dual_graph.to_json(graph_path)
