import json as json
import sys
import os
from decimal import *
import numpy as np


processed_path = './output/ga/processed'

distList = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14']
nameList = ["Presidential D", "Presidential R", 'Senate D','Senate R', "African American",'White','Asian','Hispanic','Non-Hispanic','Af. Amer. VAP','White VAP','Asian VAP','Hispanic VAP','Non-Hisp. VAP']
# PresD = {'01':[],'02':[],'03':[],'04':[],'05':[],'06':[],'07':[],'08':[],'09':[],'10':[],'11':[],'12':[],'13':[],'14':[],'15':[],'16':[],'17':[],'18':[], '19':[]}
PresD=[[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]

# numPlans = len(os.listdir(processed_path))
# print(numPlans / 2)
# x =[]
# for i in range(numPlans):
#     for j in range(numPlans):
#         x.append(j+1)
# print(x)
distList2 = []
for i in range(len(distList)):
    for j in range(5):
        distList2.append(distList[i])
print(distList2)

for item in os.listdir(processed_path):
        if os.path.isfile(os.path.join(processed_path, item)) and item.startswith('processed_'):
            plan_file = open(os.path.join(processed_path, item), "r")
            plan_data = json.load(plan_file)
            for dist in list(plan_data['percents']):
                PresD[int(dist)-1].append(float(plan_data["percents"][dist]['PresD']))
                # print(PresD)

print(PresD)
for dist in PresD:
    if dist!=[]:
        print(dist)
        med = np.percentile(dist, 50)
        third = np.percentile(dist, 75)
        first = np.percentile(dist, 25)
        mini = min(dist)
        maxi = max(dist)
        dist.clear()
        dist.append(med)
        dist.append(third)
        dist.append(first)
        dist.append(mini)
        dist.append(maxi)
print(PresD)


boxWhisk = {}
boxWhisk["names"]=nameList
boxWhisk['distList']=distList2
boxWhisk['PresD'] = PresD
print(boxWhisk)
file_name = "../../client/src/data/boxWhisk.json"
file1 = open(file_name, "w")
json.dump(boxWhisk, file1)
