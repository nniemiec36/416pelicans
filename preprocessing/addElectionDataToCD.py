import sys
import json as json

cd_pop_path = './nc-cd-pop.json'
cd_og_path = '../client/src/data/north-carolina/Original Data Files/nc_cd.json'
prec_path = '../client/src/data/north-carolina/processed/NC_PRECINCTS_ALL_INFO.json'

cd_pop_file = open(cd_pop_path, "r")
cd_og_file = open(cd_og_path, "r")
prec_file = open(prec_path, "r")

cd_pop_data = json.load(cd_pop_file)
cd_og_data = json.load(cd_og_file)
prec_data = json.load(prec_file)

newCDJSON= {"type": "FeatureCollection", "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::4269" } },"features": []}

for i in range(len(cd_pop_data["features"])):
    TOTALPOP =cd_pop_data["features"][i]["TOTALPOP"]
    TOTALHIS = cd_pop_data["features"][i]["TOTALHIS"]
    TOTALNONHIS = cd_pop_data["features"][i]["TOTALNONHIS"]
    TOTALASIAN = cd_pop_data["features"][i]["TOTALASIAN"]
    TOTALWHITE = cd_pop_data["features"][i]["TOTALWHITE"]
    TOTALAA = cd_pop_data["features"][i]["TOTALAA"]
    TOTALVAP = cd_pop_data["features"][i]["TOTALVAP"]
    TOTALVAPHIS = cd_pop_data["features"][i]["TOTALVAPHIS"]
    TOTALVAPNONHIS = cd_pop_data["features"][i]["TOTALVAPNONHIS"]
    TOTALVAPASIAN = cd_pop_data["features"][i]["TOTALVAPASIAN"]
    TOTALVAPWHITE = cd_pop_data["features"][i]["TOTALVAPWHITE"]
    TOTALVAPAA = cd_pop_data["features"][i]["TOTALVAPAA"]
    DistID = cd_pop_data["features"][i]['CD116FP']
    for j in range(len(cd_og_data['features'])):
        DistID2 = cd_og_data['features'][j]['properties']['CD116FP']
        if(DistID2 == DistID):     
            rowjson = {"type": "Feature", "properties":{}}
            rowjson['properties']['DistrictID'] = DistID
            rowjson['properties']['State'] = 'NC'
            rowjson['properties']['GeoID'] = cd_og_data['features'][j]['properties']["GEOID"]

            rowjson['properties']['TOTALPOP']=TOTALPOP
            rowjson['properties']['TOTALHIS']=TOTALHIS
            rowjson['properties']['TOTALNONHIS']=TOTALNONHIS
            rowjson['properties']['TOTALASIAN']=TOTALASIAN
            rowjson['properties']['TOTALWHITE']=TOTALWHITE
            rowjson['properties']['TOTALAA']=TOTALAA
            rowjson['properties']['TOTALVAP']=TOTALVAP
            rowjson['properties']['TOTALVAPHIS']=TOTALVAPHIS
            rowjson['properties']['TOTALVAPNONHIS']=TOTALVAPNONHIS
            rowjson['properties']['TOTALVAPASIAN']=TOTALVAPASIAN
            rowjson['properties']['TOTALVAPWHITE']=TOTALVAPWHITE
            rowjson['properties']['TOTALVAPAA']=TOTALVAPAA
            #print(rowjson)
            rowjson['geometry']=cd_og_data['features'][j]['geometry']
            newCDJSON['features'].append(rowjson)
            # print(newCDJSON)
            
            


#"PRESREP": 1996, "PRESDEM": 469, "PRESTOT": 2484, "SENATER": 1950, "SENATED": 425, "SENATETOT"
for i in range(len(newCDJSON['features'])):
    distID = newCDJSON['features'][i]['properties']['DistrictID']
    PRESREP = 0
    PRESDEM = 0
    PRESTOT = 0
    SENATER = 0
    SENATED = 0
    SENATETOT = 0
    for j in range(len(prec_data['features'])):
        prec = prec_data['features'][j]['properties']
        precDist = prec['DistrictID']
        if(distID==precDist):
            PRESREP += prec['PRESREP']
            PRESDEM += prec['PRESDEM']
            PRESTOT += prec['PRESTOT']
            SENATER += prec['SENATER']
            SENATED += prec['SENATED']
            SENATETOT += prec['SENATETOT']
    newCDJSON['features'][i]['properties']['PRESREP'] = PRESREP
    newCDJSON['features'][i]['properties']['PRESDEM'] = PRESDEM
    newCDJSON['features'][i]['properties']['PRESTOT'] = PRESTOT
    newCDJSON['features'][i]['properties']['SENATER'] = SENATER
    newCDJSON['features'][i]['properties']['SENATED'] = SENATED
    newCDJSON['features'][i]['properties']['SENATETOT'] = SENATETOT
    print(newCDJSON['features'][i]['properties'])
        

file_name = "NC_CD_ALL_INFO.geojson"
file1 = open(file_name, "w")
json.dump(newCDJSON, file1)