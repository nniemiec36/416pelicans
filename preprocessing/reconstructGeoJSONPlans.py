import json as json
# import mapshaper as map
#import geopandas as gpd
import sys

list_dist = {"A":"01", "B":"02", "C":"03", "D":"04", "E":"05", "F":"06", "G":"07", "H":"08", "I":"09", "J":"10", "K":"11", "L":"12", "M":"13", "N":"14", "O":"15", "P":"16", "Q":"17", "R":"18"}


geo_path = './GA_PRECINCTS_ALL_INFO.geojson'
plan_path = sys.argv[1]

geo_file = open(geo_path, "r")
plan_file = open(plan_path, "r")

geo_data = json.load(geo_file)
plan_data = json.load(plan_file)
print(geo_data['features'][0]['properties']['DistrictID'])

# print(geo_data['features'][0])
for i in range(len(geo_data['features'])):
    geoPrecinct = geo_data['features'][i]['properties']['PrecinctID']
    for j in range(len(plan_data['nodes'])):
        planPrecinct = plan_data['nodes'][j]['id']
        if(geoPrecinct==planPrecinct):
            planDistrict = plan_data['nodes'][j]['district']
            if(planDistrict=='A'):
                planDistrict = '01'
            elif (planDistrict=='B'):
                planDistrict = '02'
            elif (planDistrict=='C'):
                planDistrict = '03'
            elif (planDistrict=='D'):
                planDistrict = '04'
            elif (planDistrict=='E'):
                planDistrict = '05'
            elif (planDistrict=='F'):
                planDistrict = '06'
            elif (planDistrict=='G'):
                planDistrict = '07'
            elif (planDistrict=='H'):
                planDistrict = '08'
            elif (planDistrict=='I'):
                planDistrict = '09'
            elif (planDistrict=='J'):
                planDistrict = '10'
            elif (planDistrict=='K'):
                planDistrict = '11'
            elif (planDistrict=='L'):
                planDistrict = '12'
            elif (planDistrict=='M'):
                planDistrict = '13'
            elif (planDistrict=='N'):
                planDistrict = '14'
            elif (planDistrict=='O'):
                planDistrict = '15'
            elif (planDistrict=='P'):
                planDistrict = '16'
            elif (planDistrict=='Q'):
                planDistrict = '17'
            elif (planDistrict=='R'):
                planDistrict = '18'
            elif (planDistrict=='S'):
                planDistrict = '19'
            geo_data['features'][i]['properties']['DistrictID'] = planDistrict

file_name = "plan_reformatted2.geojson"
file1 = open(file_name, "w")
json.dump(geo_data, file1)