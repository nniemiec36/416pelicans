import json as json
import sys

if len(sys.argv) != 3:
    print("provide the path of the json file you are trying to convert.")
    exit(0)
if "graph" in sys.argv[1]:
    exit(0)

census_path = sys.argv[1]
precinct_path = sys.argv[2]
print(census_path)
print(precinct_path)

if "GA_CENSUS" in census_path:
    print("georgia true")
    file_name = "./ga-prec-pop.json"
elif "NC_CENSUS" in census_path: 
    print("north carolina true")
    file_name = "./nc-prec-pop.json"
else:
    file_name = "./pa-prec-pop.json"

file1 = open(census_path, "r")
file2 = open(precinct_path, "r")

data = json.load(file1)
prec_data = json.load(file2)

int_id = data['features'][0]['properties']['PRECINCTID']
print(int_id)
print(int(int_id))
print(prec_data['features'][int(int_id)]['properties'])
print("break")
print(range(len(prec_data['features'])))
# print(prec_data['features'][2696]['properties'])
# print(prec_data['features'][0]['properties'])
# print(data['features'][107097]['properties']['PRECINCTID'])
# 107097
# 2697
# 207402

for i in range(len(prec_data['features'])):
    prec_data['features'][i]['properties']['TOTALPOP'] += 0
    prec_data['features'][i]['properties']['TOTALHIS'] += 0
    prec_data['features'][i]['properties']['TOTALNONHIS'] += 0
    prec_data['features'][i]['properties']['TOTALASIAN'] += 0
    prec_data['features'][i]['properties']['TOTALWHITE'] += 0
    prec_data['features'][i]['properties']['TOTALAA'] += 0
    prec_data['features'][i]['properties']['TOTALVAP'] += 0
    prec_data['features'][i]['properties']['TOTALVAPHIS'] += 0
    prec_data['features'][i]['properties']['TOTALVAPNONHIS'] += 0
    prec_data['features'][i]['properties']['TOTALVAPASIAN'] += 0
    prec_data['features'][i]['properties']['TOTALVAPWHITE'] += 0
    prec_data['features'][i]['properties']['TOTALVAPAA'] += 0

for i in (range(0, int(len(data['features'])/2))):
    int_id = data['features'][i]['properties']['PRECINCTID']
    prec_data['features'][int(int_id)]['properties']['TOTALPOP'] += data['features'][i]['properties']['TOTALPOP']
    prec_data['features'][int(int_id)]['properties']['TOTALHIS'] += data['features'][i]['properties']['TOTALHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALNONHIS'] += data['features'][i]['properties']['TOTALNONHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALASIAN'] += data['features'][i]['properties']['TOTALASIAN']
    prec_data['features'][int(int_id)]['properties']['TOTALWHITE'] += data['features'][i]['properties']['TOTALWHITE']
    prec_data['features'][int(int_id)]['properties']['TOTALAA'] += data['features'][i]['properties']['TOTALAA']
    prec_data['features'][int(int_id)]['properties']['TOTALVAP'] += data['features'][i]['properties']['TOTALVAP']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPHIS'] += data['features'][i]['properties']['TOTALVAPHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPNONHIS'] += data['features'][i]['properties']['TOTALVAPNONHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPASIAN'] += data['features'][i]['properties']['TOTALVAPASIAN']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPWHITE'] += data['features'][i]['properties']['TOTALVAPWHITE']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPAA'] += data['features'][i]['properties']['TOTALVAPAA']

for i in (range(int(len(data['features'])/2), int(len(data['features'])))):
    int_id = data['features'][i]['properties']['PRECINCTID']
    print(i)
    print(int_id)
    print(len(data['features']))
    prec_data['features'][int(int_id)]['properties']['TOTALPOP'] += data['features'][i]['properties']['TOTALPOP']
    prec_data['features'][int(int_id)]['properties']['TOTALHIS'] += data['features'][i]['properties']['TOTALHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALNONHIS'] += data['features'][i]['properties']['TOTALNONHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALASIAN'] += data['features'][i]['properties']['TOTALASIAN']
    prec_data['features'][int(int_id)]['properties']['TOTALWHITE'] += data['features'][i]['properties']['TOTALWHITE']
    prec_data['features'][int(int_id)]['properties']['TOTALAA'] += data['features'][i]['properties']['TOTALAA']
    prec_data['features'][int(int_id)]['properties']['TOTALVAP'] += data['features'][i]['properties']['TOTALVAP']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPHIS'] += data['features'][i]['properties']['TOTALVAPHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPNONHIS'] += data['features'][i]['properties']['TOTALVAPNONHIS']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPASIAN'] += data['features'][i]['properties']['TOTALVAPASIAN']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPWHITE'] += data['features'][i]['properties']['TOTALVAPWHITE']
    prec_data['features'][int(int_id)]['properties']['TOTALVAPAA'] += data['features'][i]['properties']['TOTALVAPAA']

print(file_name)
file3 = open(file_name, "w")
json.dump(prec_data, file3)