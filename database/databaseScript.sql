USE pelicans;

CREATE TABLE TotalPop(
	TotalPopID INTEGER,
    TotalPop INTEGER,
    TotalVAPop INTEGER,
    TotalCVAPop INTEGER,
	PRIMARY KEY (TotalPopID)
);

CREATE TABLE WhitePop(
	WhitePopID INTEGER,
    WhitePop INTEGER,
    WhiteVAPop INTEGER,
    WhiteCVAPop INTEGER,
	PRIMARY KEY (WhitePopID)
);

CREATE TABLE AfAmerPop(
	AfAmerPopID INTEGER,
    AfAmerPop INTEGER,
    AfAmerVAPop INTEGER,
    AfAmerCVAPop INTEGER,
	PRIMARY KEY (AfAmerPopID)
);

CREATE TABLE AsianPop(
	AsianPopID INTEGER,
    AsianPop INTEGER,
    AsianVAPop INTEGER,
    AsianCVAPop INTEGER,
	PRIMARY KEY (AsianPopID)
);

CREATE TABLE HispPop(
	HispPopID INTEGER,
    HispPop INTEGER,
    HispVAPop INTEGER,
    HispCVAPop INTEGER,
	PRIMARY KEY (HispPopID)
);

CREATE TABLE Population(
	PopulationID INTEGER,
    TotalPopID INTEGER,
    WhitePopID INTEGER,
    AfAmerPopID INTEGER,
    AsianPopID INTEGER,
    HispPopID INTEGER,
    DemocraticVotes INTEGER,
    RepublicanVotes INTEGER,
	PRIMARY KEY (PopulationID),
    FOREIGN KEY (WhitePopID) REFERENCES WhitePop(WhitePopID),
    FOREIGN KEY (AfAmerPopID) REFERENCES AfAmerPop(AfAmerPopID),
    FOREIGN KEY (AsianPopID) REFERENCES AsianPop(AsianPopID),
    FOREIGN KEY (HispPopID) REFERENCES HispPop(HispPopID)
);

CREATE TABLE DistrictPlan(
	PlanID INTEGER,
    State VARCHAR(20), #Update to make State a foreign key after state table created
    IsInteresting BOOLEAN,
    Boundary LONGTEXT,
	PRIMARY KEY (PlanID) 
);

CREATE TABLE State(
	State VARCHAR(20),
	Population INTEGER,
    EnactedDistrictPlan INTEGER,
    Boundary LONGTEXT,
	PRIMARY KEY (State),
    FOREIGN KEY (Population) REFERENCES Population(PopulationID),
    FOREIGN KEY (EnactedDistrictPlan) REFERENCES DistrictPlan(PlanID)
);

CREATE TABLE District(
	DistrictNum INTEGER,
    State VARCHAR(20),
    DistrictPlanID INTEGER,
	Population INTEGER,
    GeoID INTEGER,
    Boundary LONGTEXT,
	PRIMARY KEY (DistrictNum),
    FOREIGN KEY (State) REFERENCES State(State),
    FOREIGN KEY (DistrictPlanID) REFERENCES DistrictPlan(PlanID),
    FOREIGN KEY (Population) REFERENCES Population(PopulationID)
);

CREATE TABLE Precinct(
	PrecinctID INTEGER,
    State VARCHAR(20),
    District INTEGER,
	Population INTEGER,
    GeoID INTEGER,
    Boundary LONGTEXT,
	PRIMARY KEY (PrecinctID),
    FOREIGN KEY (State) REFERENCES State(State),
    FOREIGN KEY (District) REFERENCES District(DistrictNum),
    FOREIGN KEY (Population) REFERENCES Population(PopulationID)
);

CREATE TABLE CensusBlock(
	BlockID INTEGER,
    State VARCHAR(20),
    District INTEGER,
    Precinct INTEGER,
	Population INTEGER,
    GeoID INTEGER,
    Boundary LONGTEXT,
	PRIMARY KEY (BlockID),
    FOREIGN KEY (State) REFERENCES State(State),
    FOREIGN KEY (District) REFERENCES District(DistrictNum),
    FOREIGN KEY (Precinct) REFERENCES Precinct(PrecinctID),
    FOREIGN KEY (Population) REFERENCES Population(PopulationID)
);

CREATE TABLE BoxData(
	BoxID INTEGER,
    State VARCHAR(20),
    Min INTEGER,
    Max INTEGER, 
    Median INTEGER,
    FirstQuartile INTEGER,
    ThirdQuartile INTEGER,
    ComparisonBasis VARCHAR(40),
	PRIMARY KEY (BoxID),
    FOREIGN KEY (State) REFERENCES State(State)
);

ALTER TABLE DistrictPlan ADD CONSTRAINT	fk_State FOREIGN KEY (State) REFERENCES State(State);

#INSERT INTO mytable(PrimaryKey,Subj,CRS,Cmp,Sctn,Days,Start_Time,End_Time,Mtg_start_Date,Mtg_End_Date,Duration,Instruction_Mode,Building,Room,Instr,Enrl_Cap,Wait_Cap,Cmbnd_Descr,Cmbnd_Enrl_Cap,Course_Name,start,end) VALUES ('CSE101.1','CSE',101,'LEC','1','MWF','11:00:00','11:53:00','1/25/2021 0:00','5/19/2021 0:00',53,'In Person','TBA','TBA','Kevin McDonnell',210,70,'N/A','N/A','Introduction to Computers','11:00 AM','11:53 AM');
