import React from "react";
import './LeftPanel.css';
import $ from 'jquery';

// prevents the user from manually entering a number > max or < min (+-10 of given)
$(function () {
    $('.panelInput').change(function() {
        var max = parseFloat($(this).attr('max'));
        var min = parseFloat($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        } 
    });      
}); 

// closes the left panel
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

// created the left panel component
const LeftPanel = () => {
  return (
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onClick={closeNav} >&times;</a>
        <br/>
        <a id='chosen-plan'></a>
        <div class = "slidecontainer" >
            <div>
                <p>Population Equality:</p>
                <input class="panelInput" type="number"  placeholder="0" step="0.1" id='pop-eq-input' ></input>
            
                <p>Polsby Popper:</p>
                <input class="panelInput" type="number"  placeholder="0" step="0.1" id='graph-comp-input'></input>

                <p>Efficiency Gap:</p>
                <input class="panelInput" type="number"  placeholder="0" step="0.1" id='rac-dev-input'></input>

                <p>Minority-Majority Threshold:</p>
                <input class="panelInput" type="number"  placeholder="0" step="0.1" id='min-maj-input' ></input>
            </div>
        </div>
        
        <button id = "generate" type="button" class="generate_button" onclick="this.classList.toggle('button--loading')">
            <span class="button__text">Generate</span>
        </button>
      </div>        
  );
};

export default LeftPanel;