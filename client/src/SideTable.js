import React from 'react';
import './SideTable.css';
import {map, currentState, populationVar} from './Map';
import Plotly from 'plotly.js-dist';
import boxData from '../src/data/boxWhisk.json'
import { testSetGenerate, testGetGenerate, testUserParameters } from './FetchCalls';

export function openTable() {
    document.getElementById("table").style.background = "rgba(255, 255, 255, 0.3)";
    document.getElementById('tableLinks').style.display = 'block';
    document.getElementById('State').style.display = 'block';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    
    document.getElementById('boxPlotContainer').style.display = 'none';
    //fillTable();
    
    fillTable();
    // testing();
    // testingPost();
    setPopulationVariable();
}

export function closeTable() {
    document.getElementById("table").style.background = "rgba(255, 255, 255, 0)";
    document.getElementById('tableLinks').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
}

export function openStateTable() {
    document.getElementById('State').style.display = 'block';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
    fillTable();
}

export function openBoxPlot() {
    document.getElementById('boxPlotContainer').style.display = 'block';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    boxPlot();
    testSetGenerate();
    testUserParameters();
}

export function openDistrictTable() {
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'block';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
    testGetGenerate();
}

export function openDistrictInfo() {
    document.getElementById('Comparison').style.display = 'none';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'block';
    document.getElementById('boxPlotContainer').style.display = 'none';
    fillDistrictTable();
}
  
export function openComparisonTable() {
    document.getElementById('Comparison').style.display = 'block';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
    document.getElementById('DistrictInfo').style.display = 'none';
    document.getElementById('boxPlotContainer').style.display = 'none';
}

export function fillDistrictTable() {
    const url = "http://localhost:8080/states/getDistrictTableInfo/" + currentState 
                + "/" + populationVar;
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        fillDistrictLoop(logged);
    });
}

/* Fill Table */
// export function fillTable() {
//     const url = "http://localhost:8080/states/getTableInfo/" + currentState
//     + "/" + populationVar;
//     fetch(url)
//       .then(res => res.json())
//       .then(logged => {
//         fillLoop(logged);
//       });
//   }

  export function testing() {
    const url = "http://localhost:8080/states/test/";
    fetch(url)
      .then(res => res.text())
      .then(logged => {
        console.log(logged);
      });
  }

  export function setPopulationVariable() {
    fetch('http://localhost:8080/states/populationVariable/set/', {
        method: 'POST', // or 'PUT'
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title: populationVar})
        // body: { title: populationVar}
    })
    .then(response => response.text())
    .then(data => {
        console.log('population_variable_sucess!!' , data);
    });
}

  export function testingPost() {
    fetch('http://localhost:8080/generate/sendTest/', {
        method: 'POST', // or 'PUT'
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({title: "React POST Request Example"})
    })
    .then(response => response.text())
    .then(data => {
        console.log('Success!!! ' , data);
    });
}

  function fillDistrictLoop(arr) {
    console.log(arr);
    console.log(Object.values(arr))
    var jsonData = arr
    var total = 13;
    if(currentState === 'ga'){
        document.getElementById('distr-14').style.display = 'table-row';
        total = 14;
    } else if(currentState === 'pa'){
        document.getElementById('distr-14').style.display = 'table-row';
        document.getElementById('distr-15').style.display = 'table-row';
        document.getElementById('distr-16').style.display = 'table-row';
        document.getElementById('distr-17').style.display = 'table-row';
        document.getElementById('distr-19').style.display = 'table-row';
        total = 18;
    }

    var arrAy = Object.values(arr);
    console.log(arrAy[0]);
    console.log(arrAy.length);
    console.log(arrAy[0]['TOTALPOP']);

    jsonData = Object.values(jsonData);
    console.log("jsonData: " + jsonData);
    var i = 0;
    var popMeasure = '';
    if(populationVar === 'vap') popMeasure = populationVar.toUpperCase();
    for(var y = 0; y < total; y++){
        //var str = 'distr-' + (y+1);
        var polArr = document.getElementsByClassName('distInfo');
        polArr[i+0].innerText = jsonData[y]['CD116FP'];
        console.log(jsonData[y]['CD116FP'])
        if(populationVar === 'vap')
            polArr[i+1].innerText = jsonData[y]['TOTALVAP'].toLocaleString();
        else
            polArr[i+1].innerText = jsonData[y]['TOTALPOP'].toLocaleString();
        console.log(i+1);
        console.log(polArr[i+1]);
        polArr[i+2].innerText = jsonData[y]['TOTAL'+popMeasure+'WHITE'].toLocaleString();
        polArr[i+3].innerText = jsonData[y]['TOTAL'+popMeasure+'AA'].toLocaleString();
        polArr[i+4].innerText = jsonData[y]['TOTAL'+popMeasure+'ASIAN'].toLocaleString();
        polArr[i+5].innerText = jsonData[y]['TOTAL'+popMeasure+'HIS'].toLocaleString();
        polArr[i+6].innerText = jsonData[y]['TOTAL'+popMeasure+'NONHIS'].toLocaleString();
        i += 7;
    }
  }

export function fillTable(){
    const url = "http://localhost:8080/states/getPopData/" + currentState;
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log(logged);
        console.log(logged.populationData['totalPopulation']);
        var json = getPercentages(logged);
        json = constructJSONTable(json);
        fillLoop(json);
    });
}

export function getPercentages(arr) {
    console.log("populationVar: " + populationVar);
    if(populationVar === 'total'){
        console.log("total true");
        var jsonArr = {
            "totalPopulation": 0,
            "blackPerc": 0,
            "totalBlack": 0,
            "whitePerc": 0,
            "totalWhite": 0,
            "hisPerc": 0,
            "totalHis": 0,
            "nonHisPerc": 0,
            "totalNonHis": 0,
            "asianPerc": 0,
            "totalAsian": 0,
            "totalPresVotes": 0,
            "totalDemPresVotes": 0,
            "totalDemPresPerc": 0,
            "totalRepPresVotes": 0,
            "totalRepPresPerc": 0
        }
        jsonArr['totalPopulation'] = arr.populationData['totalPopulation'];
        console.log(jsonArr['totalPopulation']);
        jsonArr['blackPerc'] = parseFloat((arr.populationData['totalBlack'] / jsonArr['totalPopulation'])*100).toFixed(2);
        console.log(jsonArr['blackPerc']);
        jsonArr['totalBlack'] = arr.populationData['totalBlack'];
        jsonArr['whitePerc'] = parseFloat((arr.populationData['totalWhite'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalWhite'] = arr.populationData['totalWhite'];
        jsonArr['hisPerc'] = parseFloat((arr.populationData['totalHis'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalHis'] = arr.populationData['totalHis'];
        jsonArr['nonHisPerc'] = parseFloat((arr.populationData['totalNonHis'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalNonHis'] = arr.populationData['totalNonHis'];
        jsonArr['asianPerc'] = parseFloat((arr.populationData['totalAsian'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalAsian'] = arr.populationData['totalAsian'];
        jsonArr['totalPresVotes'] =  arr.populationData['presTotalVotes'];
        jsonArr['totalDemPresVotes'] = arr.populationData['demPresTotalVotes'];
        jsonArr['totalDemPresPerc'] = parseFloat((arr.populationData['demPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
        jsonArr['totalRepPresVotes'] = arr.populationData['repPresTotalVotes'];
        jsonArr['totalRepPresPerc'] = parseFloat((arr.populationData['repPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
    } else {
        var jsonArr = {
            "totalPopulation": 0,
            "blackPerc": 0,
            "totalBlack": 0,
            "whitePerc": 0,
            "totalWhite": 0,
            "hisPerc": 0,
            "totalHis": 0,
            "nonHisPerc": 0,
            "totalNonHis": 0,
            "asianPerc": 0,
            "totalAsian": 0,
            "totalPresVotes": 0,
            "totalDemPresVotes": 0,
            "totalDemPresPerc": 0,
            "totalRepPresVotes": 0,
            "totalRepPresPerc": 0
        }
        jsonArr['totalPopulation'] = arr.populationData['totalVAPPopulation'];
        console.log(jsonArr['totalPopulation']);
        jsonArr['blackPerc'] = parseFloat((arr.populationData['blackVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        console.log(jsonArr['blackPerc']);
        jsonArr['totalBlack'] = arr.populationData['blackVAP'];
        jsonArr['whitePerc'] = parseFloat((arr.populationData['whiteVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalWhite'] = arr.populationData['whiteVAP'];
        jsonArr['hisPerc'] = parseFloat((arr.populationData['hisVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalHis'] = arr.populationData['hisVAP'];
        jsonArr['nonHisPerc'] = parseFloat((arr.populationData['nonHisVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalNonHis'] = arr.populationData['nonHisVAP'];
        jsonArr['asianPerc'] = parseFloat((arr.populationData['asianVAP'] / jsonArr['totalPopulation'])*100).toFixed(2);
        jsonArr['totalAsian'] = arr.populationData['asianVAP'];
        jsonArr['totalPresVotes'] =  arr.populationData['presTotalVotes'];
        jsonArr['totalDemPresVotes'] = arr.populationData['demPresTotalVotes'];
        jsonArr['totalDemPresPerc'] = parseFloat((arr.populationData['demPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
        jsonArr['totalRepPresVotes'] = arr.populationData['repPresTotalVotes'];
        jsonArr['totalRepPresPerc'] = parseFloat((arr.populationData['repPresTotalVotes'] / jsonArr['totalPresVotes'])*100).toFixed(2);
    }
    console.log(jsonArr);
    return jsonArr;
}

export function constructJSONTable(arr){
    var jsonArr = {
        "headers":
            {
                "total-votes": "",
                "pop-total": "",
                "state-table-header": "Georgia State (2,679 Precincts)",
                "district-table-header": "Georgia Redistrictings"
            },
        "political": [
            {
                "party": "Democratic",
                "districtNumber": "6",
                "districtPercent": "42.86%",
                "voteNumber": 0,
                "votePercent": ""
            },
            {
                "party": "Republican",
                "districtNumber": "8",
                "districtPercent": "57.14%",
                "voteNumber": 0,
                "votePercent": ""
            }
        ],
        "demographic": [
            {
                "ethnicity": "White",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "African American",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Hispanic",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Non Hispanic",
                "population": 0,
                "populationPercent": "",
            },
            {
                "ethnicity": "Asian",
                "population": 0,
                "populationPercent": "",
            }
        ],
        "district": [
            {
                "number":"1",
                "populationEquality":"87.76",
                "polsbyPopper":"11.15",
                "efficiencyGap":"83.79",
                "objFunction": "63.79"
            },
            {
                "number":"2",
                "populationEquality":"74.34",
                "polsbyPopper":"5.97",
                "efficiencyGap":"83.79",
                "objFunction": "61.73"
            },
            {
                "number":"3",
                "populationEquality":"83.25",
                "polsbyPopper":"9.85",
                "efficiencyGap":"83.79",
                "objFunction": "60.99"
            }
        ]
    }

    jsonArr.headers["total-votes"] = arr['totalPresVotes'].toLocaleString();
    jsonArr.headers["pop-total"] = arr['totalPopulation'].toLocaleString();
    jsonArr.political[0]['voteNumber'] = arr['totalRepPresVotes'].toLocaleString();
    jsonArr.political[0]['votePercent'] = arr['totalRepPresPerc'].toString() + "%";
    jsonArr.political[1]['voteNumber'] = arr['totalDemPresVotes'].toLocaleString();
    jsonArr.political[1]['votePercent'] = arr['totalDemPresPerc'].toString() + "%";
    jsonArr.demographic[0]['population'] = arr['totalWhite'].toLocaleString();
    jsonArr.demographic[0]['populationPercent'] = arr['whitePerc'].toString() + "%";
    jsonArr.demographic[1]['population'] = arr['totalBlack'].toLocaleString();
    jsonArr.demographic[1]['populationPercent'] = arr['blackPerc'].toString() + "%";
    jsonArr.demographic[2]['population'] = arr['totalHis'].toLocaleString();
    jsonArr.demographic[2]['populationPercent'] = arr['hisPerc'].toString() + "%";
    jsonArr.demographic[3]['population'] = arr['totalNonHis'].toLocaleString();
    jsonArr.demographic[3]['populationPercent'] = arr['nonHisPerc'].toString() + "%";
    jsonArr.demographic[4]['population'] = arr['totalAsian'].toLocaleString();
    jsonArr.demographic[4]['populationPercent'] = arr['asianPerc'].toString() + "%";
    console.log(jsonArr);
    return jsonArr;
}

  function fillLoop(arr) {
    console.log("arr: " + arr);
    var jsonData = arr;
    var texts = Object.values(jsonData['headers']);
    console.log("texts: " + JSON.stringify(texts));
    document.getElementById('total-votes').innerText = "Total Votes: " + texts[0];
    document.getElementById('pop-total').innerText = "Total Population: " + texts[1].toLocaleString();
    document.getElementById('state-table-header').innerText = texts[2];
    document.getElementById('district-table-header').innerText = texts[3];
    var polArr = document.getElementsByClassName('pol-1');
    var i = 0;
    var j = 0;
    var k = 0;
    var values = '';
    for (k = 0; k < 2; k++) {
      values = Object.values(jsonData.political[k]);
      for (j = 0; j < values.length; j++) {
        polArr[i].innerText = values[j];
        i++;
      }
    }
    polArr = document.getElementsByClassName('dem-1');
    i = 0;
    for (k = 0; k < 5; k++) {
      values = Object.values(jsonData.demographic[k]);
      for (j = 0; j < values.length; j++) {
        polArr[i].innerText = values[j];
        i++;
      }
    }
    polArr = document.getElementsByClassName('dist-1');
    i = 0;
    for (k = 0; k < 3; k++) {
      values = Object.values(jsonData.district[k]);
      for (j = 0; j < values.length; j++) {
        polArr[i].innerText = values[j];
        i++;
      }
    }
  }

  const getFromIndex = (array, indexes) => {
    return array.filter((element, index) => indexes.includes(index)); 
  };

function boxPlot(){
    var x = boxData.distList;
    
    var t1 = {
        y: boxData.PresD[0],
        x: x,
        name: 'Dist 1',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t2 = {
        y: boxData.PresD[1],
        x: getFromIndex(boxData.distList, [5,6,7,8,9]),
        name: 'Dist2',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t3 = {
        y: boxData.PresD[2],
        x: getFromIndex(boxData.distList, [10,11,12,13,14]),
        name: 'Dist3',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t4 = {
        y: boxData.PresD[3],
        x: getFromIndex(boxData.distList, [15,16,17,18,19]),
        name: 'Dist4',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t5 = {
        y: boxData.PresD[4],
        x: getFromIndex(boxData.distList, [20,21,22,23,24]),
        name: 'Dist5',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t6 = {
        y: boxData.PresD[5],
        x: getFromIndex(boxData.distList, [25,26,27,28,29]),
        name: 'Dist6',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var t7 = {
        y: boxData.PresD[6],
        x: getFromIndex(boxData.distList, [30,31,32,33,34]),
        name: 'Dist7',
        marker: {color: '#3D9970'},
        type: 'box'
    };

    var data = [t1,t2,t3,t4,t5,t6,t7];

    var layout = {
        yaxis: {
            title: 'Democratic Presidential',
            zeroline: false
        },
        xaxis: {
            title: 'Districts',
            zeroline: false
        },
        boxmode: 'group'
    };
    Plotly.newPlot('boxPlot', data, layout);
}

const SideTable = () => {
   return ( 
        <div class='map-overlay-table' id='table' >
            <div class='tab' id="tableLinks" style={{display:"none"}}>
                <button class="tablinks" onClick={openStateTable}>State</button>
                <button class="tablinks" onClick={openDistrictTable}>District Plan</button>
                <button class="tablinks" onClick={openDistrictInfo}>District Info</button>
                <button class="tablinks" onClick={openComparisonTable} id="compare-tab">Compare Plans</button>
                <button class="tablinks" onClick={openBoxPlot}>Box-Whisker</button>
            </div>

            <div id='State' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='state-table-header'></p>
                <table class='voting-data-css'>
                <thead>
                    <tr>
                    <th>Party</th>
                    <th># Districts</th>
                    <th>Total District %</th>
                    <th># Votes</th>
                    <th>Total Vote %</th>
                    </tr>
                </thead>
                <tr>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                </tr>
                <tr>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                    <td class='pol-1'></td>
                </tr>
                </table>
                <p class='table-total' id='total-votes'></p>
                <table class='demographic-data-css'>
                <thead>
                    <tr>
                    <th>Demographic Group</th>
                    <th>Population</th>
                    <th>Population %</th>
                    </tr>
                </thead>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                <tr>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                    <td class='dem-1'></td>
                </tr>
                </table>
                <p class="table-total" id='pop-total'></p>
            </div>

            <div id='District' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='district-table-header'></p>
                <table class='voting-data-css'>
                <thead>
                    <tr>
                    <th>Redistricting Plan #</th>
                    <th>Population Equality</th>
                    <th>Polsby Popper</th>
                    <th>Efficiency Gap</th>
                    <th>Objective Function</th>
                    </tr>
                </thead>
                <tr id='red-1'>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                </tr>
                <tr id='red-2'>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                </tr>
                <tr id='red-3'>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                    <td class='dist-1'></td>
                </tr>
                </table>
            </div>

            <div id='DistrictInfo' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='district-table-header'></p>
                <table class='district-data-css'>
                <thead>
                    <tr>
                    <th>District <br />ID</th>
                    <th>Total</th>
                    <th>White</th>
                    <th>African <br /> American</th>
                    <th>Asian</th>
                    <th>Hispanic</th>
                    <th>Non <br />Hispanic</th>
                    </tr>
                </thead>
                <tr id='distr-1'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-2'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-3'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-4'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-5'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-6'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-7'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-8'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-9'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-10'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-11'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-12'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-13'>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-14' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-15' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-16' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-17' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                <tr id='distr-18' style={{display:"none"}}>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                    <td class='distInfo'></td>
                </tr>
                </table>
            </div>

            <div id='Comparison' class="tabcontent" style={{display:"none"}}>
                <p class="table-header" id='compare-table-header'></p>
                <table class='voting-data-css'>
                <thead>
                    <tr>
                    <th>Redistricting Plan</th>
                    <th>Population Equality</th>
                    <th>Polsby Popper</th>
                    <th>Efficiency Gap</th>
                    <th>Objective Function</th>
                    </tr>
                </thead>
                <tr id='chosen' onclick="openChosenPlan()">
                    <td class='chosen-red'></td>
                    <td class='chosen-red'></td>
                    <td class='chosen-red'></td>
                    <td class='chosen-red'></td>
                    <td class='chosen-red'></td>
                </tr>
                <tr id='new-plan' onclick="openNewPlan()">
                    <td class='new-red'></td>
                    <td class='new-red'></td>
                    <td class='new-red'></td>
                    <td class='new-red'></td>
                    <td class='new-red'></td>
                </tr>
                </table>
            </div>

        <div id='boxPlotContainer' style={{display:"none"}}>
            <div id="plot-dropdown">
                <div class="plotpViewDiv" >
                    <label id='plot-view'>Plot Type</label>
                </div>
                <div>
                    <select class="form-control" class="map-view-options" id='map-view-dropdown'>
                        <option class="mapOptions" value="PresD" >Presedential Democratic</option>
                        <option class="mapOptions" value="PresR" >Presedential Republican</option>
                        <option class="mapOptions" value="SenD" >Senate Democratic</option>
                        <option class="mapOptions" value="SenR" >Senate Republican</option>
                        
                        <option class="mapOptions" value="AfAmer" >African American</option>
                        <option class="mapOptions" value="White" >White</option>
                        <option class="mapOptions" value="Asian" >Asian</option>
                        <option class="mapOptions" value="Hisp" >Hispanic</option>
                        <option class="mapOptions" value="NonHis" >Non-Hispanic</option>
                        
                        <option class="mapOptions" value="Opp" >Opportunity District</option>
                    </select>
                </div>
            <div id='boxPlot' class="tabcontent" style={{display:"block"}}>
                </div>
                </div>
            
                <p class="table-header"></p>
            </div>
        </div>
    );
};

export default SideTable;