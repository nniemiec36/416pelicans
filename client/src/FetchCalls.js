import React from "react";
import './Header.css';

export function testSetGenerate(){
    const url = "http://localhost:8080/states/putObjTest/" + "ga";
    console.log(url);
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify("var")
    })
    .then(res => res.text())
    .then(logged => {
        console.log(logged);
        console.log('Success!!! ' , logged);
    });
}

export function testGetGenerate(){
    const url = "http://localhost:8080/states/getObjTest/";
    console.log(url);
    fetch(url)
    .then(res => res.json())
    .then(logged => {
        console.log(logged);
        console.log('Success!!! ' , logged);
    });
}

export function testUserParameters(){
    // for an array
    var timeLimit = 300;
    var popEquality = .50;
    var compactness = .90;
    var minMaj = 7;
    const url = "http://localhost:8080/states/checkParameters/" + timeLimit + "/" 
    + popEquality + "/" + compactness + "/" + minMaj + "/";
    console.log(url);
    fetch(url)
    .then(res => res.text())
    .then(logged => {
        console.log(logged);
    });

}