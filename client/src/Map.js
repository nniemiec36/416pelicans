import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import './Map.css';
import { districtLegend, clearLegend, lineLegend } from './Legend';
import { redistrColors, electionRange, districtColors, georgiaMap, northCarolinaMap, pennMap, precinctLines, districtLines, stateLines, countyLines, congressElection, pennElectionLayers, pennCongElectLayers, pennDistrictLayers, northCarolinaCongElectLayers, georgiaElectionLayers, northCarolinaElectionLayers, georgiaCongElectLayers, georgiaDistrictLayers, northCarolinaDistrictLayers } from './Constants';
import stateData from './data/statesElections.geojson';
import Header from './Header';
import Legend from './Legend';
import LeftPanel from './LeftPanel';
import SideTable from './SideTable';
import gaStateTable from "./data/georgia/ga-state-table-content.json";
import paStateTable from "./data/penn/pa-state-table-content.json";
import ncStateTable from "./data/north-carolina/nc-state-table-content.json";
import { openPAMap, openPennsylvania, updatePennLayers } from './PennMap';
import { openGAMap, openGeorgia, updateGeorgiaLayers } from './GeorgiaMap';
import { openNCMap, openNorthCarolina, updateNorthCarolinaLayers } from './NCMap';
import { openTable, closeTable } from './SideTable';

mapboxgl.accessToken = 'pk.eyJ1Ijoibm5pZW1pZWMiLCJhIjoiY2t0Z2NlYTBqMGdzYjJ3cGhvNTl5dmk3NSJ9.3OkgWWwVaFcxi77btxn3Zg';

export var map;
export var currentState;
export var currentMap;
export var chosenPlan;
export var districtView;
export var precinctView;
export var countyView;
export var populationVar;
var states = ['Pennsylvania', 'Georgia', 'North Carolina'];

const Map = () => {
  map = useRef(null);
  const mapContainerRef = useRef(null);
  const [active, setActive] = useState(null);

  useEffect(() => {
    if (map.current) return;
    else {
      map.current = new mapboxgl.Map({
        container: mapContainerRef.current,
        center: [-95.7, 37.7],
        zoom: 3,
        style: 'mapbox://styles/nniemiec/cktg9vbjk177f17uipv8q58j0',
      });

      var nav = new mapboxgl.NavigationControl();
      map.current.addControl(nav, 'top-right');
      populationVar = 'total';
      console.log(populationVar);

      map.current.on('load', () => {
        addGeoJSONLayer('none', 'us_states_elections_outline', 'line', 'waterway-label', 'country', 'election', stateData, 'visible', 0, 15);
        addGeoJSONLayer('none', 'us_states_elections', 'fill', 'us_states_elections_outline', 'country', 'election', stateData, 'visible', 0, 6);
        const legend = document.getElementById('legend');
        const label = document.createElement('label');
        label.innerHTML = `Legend`;
        legend.appendChild(label);
        lineLegend();
        openPAMap();
        openGAMap();
        openNCMap();
      });
    }

    currentState = 'none';
    districtView = true;
    precinctView = true;
    countyView = true;
    currentMap = 'district-plans';
    chosenPlan = 'none';
    populationVar = 'total';

    // change cursor style to point when hovering a clickable state
    map.current.on('mouseenter', 'us_states_elections', function () {
      map.current.getCanvas().style.cursor = 'pointer';
    });

    // Change it back to a pointer when it leaves.
    map.current.on('mouseleave', 'us_states_elections', function () {
      map.current.getCanvas().style.cursor = '';
    });

    // Clicked on a state that is not Georgia, North Carolina, or Pennsylvania
    map.current.on('click', 'us_counties_elections', function (e) {
      var stateName = e.features[0].properties.State;
      if (!states.includes(stateName)) {
        resetMap();
      }
    });

    // generate new plan controls
    map.current.on('load', () => {
      var row = document.getElementById('red-1');
      row.onclick = function () {
        chosenPlan = '1';
        setInputs(1);
        console.log(currentState);
        console.log((currentState + '_congressional_districts'));
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')]
        for (const id of off) {
          map.current.setLayoutProperty(id, 'visibility', 'none');
        }
        const on = [(currentState + '_redistricting_1'), (currentState + '_redistricting_1_map')];
        for (const id of on) {
          map.current.setLayoutProperty(id, 'visibility', 'visible');
        }
      };

      row = document.getElementById('red-2');
      row.onclick = function () {
        chosenPlan = '2';
        setInputs(6);
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        console.log(currentState);
        console.log((currentState + '_congressional_districts'));
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_1'), (currentState + '_redistricting_1_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')]
        for (const id of off) {
          map.current.setLayoutProperty(id, 'visibility', 'none');
        }
        const on = [(currentState + '_redistricting_2'), (currentState + '_redistricting_2_map')];
        for (const id of on) {
          map.current.setLayoutProperty(id, 'visibility', 'visible');
        }
      };

      row = document.getElementById('red-3');
      row.onclick = function () {
        chosenPlan = '3';
        setInputs(11);
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        console.log(currentState);
        console.log((currentState + '_congressional_districts'));
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_1'), (currentState + '_redistricting_1_map')]
        for (const id of off) {
          map.current.setLayoutProperty(id, 'visibility', 'none');
        }
        const on = [(currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')];
        for (const id of on) {
          map.current.setLayoutProperty(id, 'visibility', 'visible');
        }
      };

      row = document.getElementById('chosen');
      row.onclick = function () {
        // } else if (currentState === 'nc') {
          if (chosenPlan === '1') {
            const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')]
            for (const id of off) {
              map.current.setLayoutProperty(id, 'visibility', 'none');
            }
            const on = [(currentState + '_redistricting_1'), (currentState + '_redistricting_1_map')];
            for (const id of on) {
              map.current.setLayoutProperty(id, 'visibility', 'visible');
            }
          } else if (chosenPlan === '2') {
            const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_1'), (currentState + '_redistricting_1_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')]
            for (const id of off) {
              map.current.setLayoutProperty(id, 'visibility', 'none');
            }
            const on = [(currentState + '_redistricting_2'), (currentState + '_redistricting_2_map')];
            for (const id of on) {
              map.current.setLayoutProperty(id, 'visibility', 'visible');
            }
          } else {
            const off = [(currentState + '_congressional_districts'), (currentState + '_original_districts'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_1'), (currentState + '_redistricting_1_map')]
            for (const id of off) {
              map.current.setLayoutProperty(id, 'visibility', 'none');
            }
            const on = [(currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')];
            for (const id of on) {
              map.current.setLayoutProperty(id, 'visibility', 'visible');
            }
          }
      };

      row = document.getElementById('new-plan');
      row.onclick = function () {
        document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
        const off = [(currentState + '_redistricting_1_map'), (currentState + '_redistricting_1'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')]
        for (const id of off) {
          map.current.setLayoutProperty(id, 'visibility', 'none');
        }
        const on = [(currentState + '_congressional_districts'), (currentState + '_original_districts')];
        for (const id of on) {
          map.current.setLayoutProperty(id, 'visibility', 'visible');
        }
      };
    });

    /* Clicking states */
    map.current.on('click', 'us_states_elections', function (e) {
      var stateName = e.features[0].properties.State;
      const mapView = document.getElementById('map-view-dropdown');
      const mapOption = mapView.value;
      if (stateName === 'Georgia') {
        currentState = 'ga';
      }else if(stateName==="North Carolina"){
        currentState='nc';
      }else if (stateName==='Pennsylvania'){
        currentState='pa';
      }else{
        currentState='none';
      }
      openState(currentState, mapOption);
    });
  }, []);

  function fillComparisonTable() {
    var file;
    if (currentState === 'ga') {
      file = gaStateTable;
    } else if (currentState === 'pa') {
      file = paStateTable;
    } else if (currentState === 'nc') {
      file = ncStateTable;
    } else {
    }

    var newData = {
      "number": "New Plan",
      "populationEquality": "93.76",
      "polsbyPopper": "8.13",
      "efficiencyGap": "83.79",
      "objFunction": "68.22"
    }

    var newArr = document.getElementsByClassName('new-red');
    var i = 0;
    var j = 0;
    var k = 0;
    var values = Object.values(newData);
    for (j = 0; j < values.length; j++) {
      newArr[i].innerText = values[j];
      i++;
    }

    var jsonData = {};
    jsonData = file;
    var chosenArr = document.getElementsByClassName('chosen-red');
    i = 0;
    j = 0;
    if (chosenPlan === '1') {
      k = 0;
    } else if (chosenPlan === '2') {
      k = 1;
    } else {
      k = 2;
    }
    values = Object.values(jsonData.district[k]);
    for (j = 0; j < values.length; j++) {
      chosenArr[i].innerText = values[j];
      i++;
    }
  }

  // sets the min and max of generate side panel
  function setInputs(num) {
    var values = document.getElementsByClassName('dist-1');
    var arr = [];
    for (var i = 0; i < values.length; i++) { // get number out
      arr.push(values[i].innerText);
    }
    document.getElementById('pop-eq-input').value = arr[num];
    document.getElementById('pop-eq-input').max = arr[num] - (-10);
    document.getElementById('pop-eq-input').min = arr[num] - 10;
    document.getElementById('graph-comp-input').value = arr[num + 1];
    document.getElementById('graph-comp-input').min = arr[num + 1] - 10;
    document.getElementById('graph-comp-input').max = arr[num + 1] - (-10);
    document.getElementById('rac-dev-input').value = arr[num + 2];
    document.getElementById('rac-dev-input').min = arr[num + 2] - 10;
    document.getElementById('rac-dev-input').max = arr[num + 2] - (-10);
    document.getElementById('min-maj-input').value = arr[num + 3];
    document.getElementById('min-maj-input').min = arr[num + 3] - 10;
    document.getElementById('min-maj-input').max = arr[num + 3] - (-10);
  }

  useEffect(() => {
    map.current.on('load', () => {
      const generate = document.getElementById('generate');
      generate.onclick = function () {
        const off = [(currentState + '_redistricting_1'), (currentState + '_redistricting_1_map'), (currentState + '_redistricting_2'), (currentState + '_redistricting_2_map'), (currentState + '_redistricting_3'), (currentState + '_redistricting_3_map')];
        for (const id of off) {
          map.current.setLayoutProperty(id, 'visibility', 'none');
        }
        const on = [(currentState + '_congressional_districts'), (currentState + '_original_districts')];
        for (const id of on) {
          map.current.setLayoutProperty(id, 'visibility', 'visible');
        }
        fillComparisonTable();
      }
    });
  });

  return (
    <div>
      <div ref={mapContainerRef} className='map-container' />
      <Header active={active} />
      <Legend active={active} />
      <SideTable active={active} />
      <LeftPanel active={active} />
    </div>
  );
};

function flyTo() {
  var lat;
  var lng;
  if (currentState === 'pa') {
    lat = pennMap.lat;
    lng = pennMap.lng;
  } else if (currentState === 'ga') {
    lat = georgiaMap.lat;
    lng = georgiaMap.lng;
  } else if (currentState === 'nc') {
    lat = northCarolinaMap.lat;
    lng = northCarolinaMap.lng;
  } else {
    resetMap();
    return;
  }
  map.current.flyTo({
    center: [lng, lat],
    zoom: 6
  });
}

export function openState(state, mapView) {
  currentState = state;
  flyTo(state);
  if (state === 'ga') {
    openGeorgia(mapView);
  } else if (state === 'nc') {
    openNorthCarolina(mapView);
  } else if (state === 'pa') {
    openPennsylvania(mapView);
  } else {
    resetMap();
    return;
  }
  openTable();
  districtLegend(currentState);
}

export function electionMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaElectionLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaElectionLayers);
  } else if (currentState === 'pa') {
    updatePennLayers(pennElectionLayers);
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend();
}

export function conDisMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaCongElectLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaCongElectLayers);
  } else if (currentState === 'pa') {
    updatePennLayers(pennCongElectLayers);
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend();
}

export function districtMap() {
  if (currentState === 'ga') {
    updateGeorgiaLayers(georgiaDistrictLayers);
  } else if (currentState === 'nc') {
    updateNorthCarolinaLayers(northCarolinaDistrictLayers);
  } else if (currentState === 'pa') {
    updatePennLayers(pennDistrictLayers);
    //pennDistrictMap();
  } else {
    map.current.setLayoutProperty('us_states_elections_outline', 'visibility', 'visible');
    map.current.setLayoutProperty('us_states_elections', 'visibility', 'visible');
  }
  clearLegend();
  districtLegend(currentState);
}

export function addGeoJSONLayer(state, layerID, fillType, aboveLayer, level, mapView, data, visibility, minzoom, maxzoom) {
  let paintVar = '';
  if (fillType === 'fill') {
    if (mapView === 'election') {
      paintVar = electionRange;
    } else if (mapView === 'districtColors') {
      paintVar = districtColors;
    } else if (mapView === 'cElection') {
      paintVar = congressElection;
    } else if (mapView === 'redistrColors') {
      paintVar = redistrColors;
    }
  } else { 
    if (level === 'country') {
      paintVar = stateLines;
    } else if (level === 'precinct') {
      paintVar = precinctLines;
    } else if (level === 'district') {
      paintVar = districtLines;
    } else if (level === 'county') {
      paintVar = countyLines;
    }
  }
  map.current.addLayer({
    "id": layerID,
    "type": fillType,
    source: {
      type: "geojson",
      data: data,
    },
    paint: paintVar,
    minzoom: minzoom,
    maxzoom: maxzoom,
    layout: {
      'visibility': visibility,
    },
  },
    aboveLayer
  );
}

export function resetMap() {
  currentState = 'none';
  map.current.flyTo({
    center: [-85.5, 37.7],
    zoom: 3
  });
  closeTable();
  clearLegend();
}

export function setDistrictView(flag) {
  districtView = flag;
}

export function setPrecinctView(flag) {
  precinctView = flag;
}

export function setCountyView(flag) {
  countyView = flag;
}

export function setPopVar(popVar) {
  populationVar = popVar;
  console.log(populationVar);
}

export default Map;