// import precincts from './pa_precincts.json';
import precincts from './processed/PA_PRECINCTS_GEOMETRY.json';
import districts from '../pa_cd.json';
// import census from './pa_blocks_20.json';
import fs from "fs";
import JSONStream from 'JSONStream';
import es from "event-stream";
import StreamArray from 'stream-json/streamers/StreamArray.js';
import census from './dist_and_prec_from_blocks.json';
import * as turf from '@turf/turf'

// CODE FOR GENERATING DIST AND PREC FROM CENSUS BLOCKS ONLY
var records = [];

// "pa_cb_d1.json"
// "pa_cb_d2.json"
//....
// if(cd_Did == 01)
//  write(line) to "pa_cb_d1.json"
var getStream = function () {
    var jsonData = './processed/PA_PRECINCTS_GEOMETRY.json',
        stream = fs.createReadStream(jsonData, { encoding: 'utf8' }),
        parser = JSONStream.parse('*.*');


    return stream.pipe(parser);
};


getStream()
    .pipe(es.mapSync(function (data) {
        console.log(data);
        var dist = data.properties.DISTRICTID;
        //var prec = data.properties.VTD;
        data = JSON.stringify(data);
        if(dist === '01'){
            fs.appendFileSync('./pa_prec_d1.json', data+ ", \n");
        } else if (dist === '02') {
            fs.appendFileSync('./pa_prec_d2.json', data+ ", \n");
        } else if (dist === '03') {
            fs.appendFileSync('./pa_prec_d3.json',data+ ", \n");
        } else if (dist === '04') {
            fs.appendFileSync('./pa_prec_d4.json', data+ ", \n");
        } else if (dist === '05') {
            fs.appendFileSync('./pa_prec_d5.json', data+ ", \n");
        } else if (dist === '06') {
            fs.appendFileSync('./pa_prec_d6.json', data+ ", \n");
        } else if (dist === '07') {
            fs.appendFileSync('./pa_prec_d7.json', data+ ", \n");
        } else if (dist === '08') {
            fs.appendFileSync('./pa_prec_d8.json', data+ ", \n");
        } else if (dist === '09') {
            fs.appendFileSync('./pa_prec_d9.json', data+ ", \n");
        } else if (dist === '10') {
            fs.appendFileSync('./pa_prec_d10.json', data+ ", \n");
        } else if (dist === '11') {
            fs.appendFileSync('./pa_prec_d11.json', data+ ", \n");
        } else if (dist === '12') {
            fs.appendFileSync('./pa_prec_d12.json', data+ ", \n");
        } else if (dist === '13') {
            fs.appendFileSync('./pa_prec_d13.json', data+ ", \n");
        } else if (dist === '14') {
            fs.appendFileSync('./pa_prec_d14.json', data+ ", \n");
        } else if (dist === '15') {
            fs.appendFileSync('./pa_prec_d15.json', data+ ", \n");
        } else if (dist === '16') {
            fs.appendFileSync('./pa_prec_d16.json', data+ ", \n");
        } else if (dist === '17') {
            fs.appendFileSync('./pa_prec_d17.json', data+ ", \n");
        } else {
            fs.appendFileSync('./pa_prec_d18.json', data+ ", \n");
        }
    }));
