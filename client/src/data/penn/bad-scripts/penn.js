// import precincts from './pa_precincts.json';
import precincts from '../pa_precincts_wo_multi.json';
import districts from '../pa_cd.json';
// import census from './pa_blocks_20.json';
import fs from "fs"; 
import JSONStream from 'JSONStream';
import es from "event-stream";
import StreamArray from 'stream-json/streamers/StreamArray.js';
import census from './dist_and_prec_from_blocks.json';
import * as turf from '@turf/turf'

// georgia precinct geometries (precinct id, district id, geometry)
// "STATEID", "PRECINCTID", "PRECINCTNAME", "DISTRICTID", "GEOMETRY"
var json = {type:"FeatureCollection", 
            features: []};

var pfeatures = precincts.features;
var dfeatures = districts.features;
var numIssues = 0;
var stateID = 'PA';
for(var i = 0; i < pfeatures.length; i++){
  var poly = pfeatures[i].geometry; // geometry of each row in precincts
  var polyID = pfeatures[i].properties.NAME;
  var precID = i;
  var area = turf.area(turf.polygon(poly.coordinates));
  var count = 0;
  var found = false;
  var turfErrorFlag = false;
  for(var j = 0; j < dfeatures.length; j++){
      if(turfErrorFlag)
        continue; // turft cant handle these coords
    var dist = dfeatures[j].geometry;
    var distID = dfeatures[j].properties.CD116FP;
    var overlap = null;
    try{  
        overlap = turf.intersect(poly, dist);
    }catch{
      console.log("error PREC #"+i+" name "+polyID);
      turfErrorFlag = true;
      continue;
    }

    if(overlap===null)
        continue;
    if(overlap.geometry.type=="MultiPolygon"){
          overlap = turf.multiPolygon(overlap.geometry.coordinates);
    }else{
        overlap = turf.polygon(overlap.geometry.coordinates);
    }
    overlap = turf.area(overlap);
    count++;
    var valid = (overlap/area).toPrecision(5)*100.0;
    if(valid > 60){
        found =true;
        // ADD TO JSON: "PRECINCTID", "PRECINCTNAME", "DISTRICTID", "STATEID", "GEOMETRY"
        addRowToJSON(precID, polyID, distID, stateID, poly.coordinates, json);
    }
  }
  if(found === false){
      console.log("error none at 60 for "+polyID);
      addRowToJSON(precID, polyID, null, stateID, poly.coordinates, json); // manually input dist id
  }
}
// CREATE JSON
const data = JSON.stringify(json);
// write JSON string to a file
fs.writeFile('PA_PRECINCTS_GEOMETRY.json', data, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});

function addRowToJSON(PRECINCTID, PRECINCTNAME, DISTRICTID, STATEID, Coords, json){
  var jsonRow = {type: "Feature", 
                properties:{PRECINCTID: PRECINCTID, PRECINCTNAME: PRECINCTNAME, DISTRICTID: DISTRICTID, STATEID: STATEID},
                geometry:{
                  type: "Polygon",
                    coordinates: Coords
                }
              };
  json.features.push(jsonRow);
}
 


// script for Polsby Popper: ------------------------------------
// var pfeatures =  precincts.features;
// var prec = pfeatures[1].geometry;

// var poly = turf.polygon(pfeatures[0].geometry.coordinates);
// var area2 = turf.area(poly);
// console.log(area2);

// import GeoJsonGeometriesLookup from 'geojson-geometries-lookup';

// fs.readFile('./pa_blocks_20.json', 'utf8', (err, jsonString) => {
//   if (err) {
//       console.log("File read failed:", err)
//       return
//   }
//   console.log('File data:', jsonString) 
// })

// CODE FOR GENERATING DIST AND PREC FROM CENSUS BLOCKS ONLY
// var records = [];

// var getStream = function () {
//     var jsonData = './pa_blocks_20.json',
//         stream = fs.createReadStream(jsonData, { encoding: 'utf8' }),
//         parser = JSONStream.parse('*.*');
        
            
//     return stream.pipe(parser);
// };


// getStream()
//     .pipe(es.mapSync(function (data) {
//         var dist = data.properties.CD116;
//         var prec = data.properties.VTD;
//         var rec = '{"prec": "'+prec+'", "dist": "'+dist+'"}, ';
//         fs.appendFileSync('./dist_and_prec_from_blocks.json', rec);
//     }));

// code to compare created dist & prec file from census with given prec file
// var pfeatures =  precincts.features;
// var cfeatures = census.features;
// // add to maps?
// let map = new Map();
// var count = 0;
// for(var i =0;i<cfeatures.length;i++){
//     var precID = cfeatures[i].prec;
//     var distID = cfeatures[i].dist;
//     //console.log(precID+" "+distID);
//     if(map.has(precID)){
//         if(map.get(precID)!=distID){
//             console.log("NOT SAME "+map.get(precID)+" "+distID);
//             count++;
//         }
//     }else{
//         map.set(precID, distID);
//     }
// }
// console.log(map);
// console.log(count);

// for(vgit


// var pfeatures = precincts.features;
// for(var i = 0; i < pfeatures.length; i++){
//   var precID = pfeatures[i].properties.VTDST;
//   var name = pfeatures[i].properties.NAME;
//   console.log(precID+", "+name);
// }

// var cfeatures = census.features;
// for(var i = 0; i < pfeatures.length; i++){
//   var blockPrecID = pfeatures[i].properties.VTD;
//   var blockDist = pfeatures[i].properties.CD116;
//   console.log(blockPrecID+", "+blockDist);
// }



// var dfeatures = districts.features;
// var test =  { type: "Polygon", coordinates: [ [ [ -79.950382, 40.211559 ], [ -79.949928, 40.211465 ], [ -79.949594, 40.211397 ], [ -79.948712, 40.211242 ], [ -79.947894, 40.211084 ], [ -79.947293, 40.210992 ], [ -79.945043, 40.21058 ], [ -79.94291, 40.210288 ], [ -79.939848, 40.20992 ], [ -79.938683, 40.209814 ], [ -79.938736, 40.209637 ], [ -79.938748, 40.209594 ], [ -79.93879, 40.20946 ], [ -79.938902, 40.209088 ], [ -79.938947, 40.208936 ], [ -79.939014, 40.208717 ], [ -79.939118, 40.208442 ], [ -79.939433, 40.20762 ], [ -79.939497, 40.207453 ], [ -79.939597, 40.207355 ], [ -79.939761, 40.207197 ], [ -79.939782, 40.207179 ], [ -79.939934, 40.20703 ], [ -79.940544, 40.207112 ], [ -79.94057, 40.206998 ], [ -79.940587, 40.206926 ], [ -79.940629, 40.206831 ], [ -79.940756, 40.206546 ], [ -79.940799, 40.206452 ], [ -79.940838, 40.206334 ], [ -79.940956, 40.205984 ], [ -79.940996, 40.205867 ], [ -79.94105, 40.205766 ], [ -79.941071, 40.205729 ], [ -79.941142, 40.205439 ], [ -79.94117, 40.205329 ], [ -79.941217, 40.205265 ], [ -79.94136, 40.205076 ], [ -79.941408, 40.205014 ], [ -79.941494, 40.204761 ], [ -79.941514, 40.204726 ], [ -79.941834, 40.20423 ], [ -79.942078, 40.203848 ], [ -79.942284, 40.203287 ], [ -79.942376, 40.202887 ], [ -79.94246, 40.202055 ], [ -79.94246, 40.201445 ], [ -79.942521, 40.200838 ], [ -79.942558, 40.200373 ], [ -79.942986, 40.199957 ], [ -79.943612, 40.199446 ], [ -79.944092, 40.199095 ], [ -79.944696, 40.198725 ], [ -79.944807, 40.198681 ], [ -79.944893, 40.198647 ], [ -79.945655, 40.198342 ], [ -79.945895, 40.198247 ], [ -79.94593, 40.19823 ], [ -79.946511, 40.198183 ], [ -79.947274, 40.197912 ], [ -79.948014, 40.197607 ], [ -79.948617, 40.197367 ], [ -79.949242, 40.197286 ], [ -79.949837, 40.197317 ], [ -79.950745, 40.197653 ], [ -79.951775, 40.198213 ], [ -79.952813, 40.198438 ], [ -79.953385, 40.198629 ], [ -79.95443, 40.198759 ], [ -79.955033, 40.198824 ], [ -79.955643, 40.198919 ], [ -79.956696, 40.199144 ], [ -79.957261, 40.19922 ], [ -79.957854, 40.199351 ], [ -79.957901, 40.199828 ], [ -79.958033, 40.201171 ], [ -79.958095, 40.201194 ], [ -79.958108, 40.201288 ], [ -79.959091, 40.201709 ], [ -79.958897, 40.201791 ], [ -79.958606, 40.2019 ], [ -79.958586, 40.201906 ], [ -79.958414, 40.201962 ], [ -79.958046, 40.202077 ], [ -79.95617, 40.202668 ], [ -79.955943, 40.202742 ], [ -79.95583, 40.202788 ], [ -79.955721, 40.202838 ], [ -79.955616, 40.202894 ], [ -79.955515, 40.202954 ], [ -79.95542, 40.203019 ], [ -79.95533, 40.203089 ], [ -79.955246, 40.203162 ], [ -79.954786, 40.203591 ], [ -79.954244, 40.204095 ], [ -79.95409, 40.204219 ], [ -79.954015, 40.204278 ], [ -79.953791, 40.204457 ], [ -79.953717, 40.204517 ], [ -79.953554, 40.204648 ], [ -79.953155, 40.20497 ], [ -79.953066, 40.205041 ], [ -79.952904, 40.205172 ], [ -79.951936, 40.206537 ], [ -79.951362, 40.20757 ], [ -79.951059, 40.20865 ], [ -79.950986, 40.208913 ], [ -79.950805, 40.209675 ], [ -79.950616, 40.210478 ], [ -79.950382, 40.211559 ] ] ] };
// for(var j = 0; j < dfeatures.length; j++){
//   var dist = dfeatures[j].geometry;
//   var distID = dfeatures[j].properties.CD116FP;
//   const glookup = new GeoJsonGeometriesLookup(dist);
//   var x = glookup.hasContainers(test);
//   console.log(x);
//   if(x===true){
//     console.log(polyID+" is in "+distID);
//     set.add(distID);
//   }
// }

