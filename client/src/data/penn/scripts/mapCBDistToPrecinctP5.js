import precincts from './processed/PA_PRECINCTS_P4.json';
//import census1 from './PA_CENSUS_1.json';
//import census2 from './PA_CENSUS_2.json';
//import census3 from './PA_CENSUS_3.json';
//import census4 from './PA_CENSUS_4.json';
import census5 from './processed/broken-up-data/PA_CENSUS_5.json';
import fs from "fs";
//console.log(census1);

for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    var precFoundC5 = false;
    for(var j = 0; j < census5.features.length; j++){
        var cfeat = census5.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        if(precinctGeoid === cb_id){
            precFoundC5 = true;
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./processed/PA_PRECINCTS.json', row + ", \n");
            break;
        }
    }

    if(precFoundC5 === false){
        var row = JSON.stringify(precincts.features[i]);
        fs.appendFileSync('./processed/PA_PRECINCTS.json', row + ", \n");
    }
}

console.log("done");
