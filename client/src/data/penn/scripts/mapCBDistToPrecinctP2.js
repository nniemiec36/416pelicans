import precincts from './processed/PA_PRECINCTS_P1.json';
//import census1 from './PA_CENSUS_1.json';
import census2 from '../processed/broken-up-data/PA_CENSUS_2.json';
//import census3 from './PA_CENSUS_3.json';
//import census4 from './PA_CENSUS_4.json';
import fs from "fs";
//console.log(census1);
console.log(precincts.features.length);
for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    //var precFoundC1 = false;
    var precFoundC2 = false;
    for(var j = 0; j < census2.features.length; j++){
        var cfeat = census2.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        //console.log(cb_id);
        //console.log(precinctGeoid);
        if(precinctGeoid === cb_id){
            precFoundC2 = true;
            //console.log("true");
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./processed/PA_PRECINCTS_P2.json', row + ", \n");
            break;
        }
    }

    if(precFoundC2 === false){
        var row = JSON.stringify(precincts.features[i]);
        fs.appendFileSync('./processed/PA_PRECINCTS_P2.json', row + ", \n");
    }
}

console.log("done");
