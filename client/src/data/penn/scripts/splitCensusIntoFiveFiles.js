import fs from "fs";
import JSONStream from 'JSONStream';
import es from "event-stream";

var getStream = function () {
    var jsonData = './processed/PA_CENSUS.json',
        stream = fs.createReadStream(jsonData, { encoding: 'utf8' }),
        parser = JSONStream.parse('*.*');
    return stream.pipe(parser);
};

var count = 0;
getStream()
    .pipe(es.mapSync(function (data) { // for each census block
        //console.log(JSON.stringify(data));
        var row = JSON.stringify(data);
        if(count < 75000){
            fs.appendFileSync('./PA_CENSUS_1.json', row + ", \n");
        } else if(count < 150000){
            fs.appendFileSync('./PA_CENSUS_2.json', row + ", \n");
        } else if(count < 225000){
            fs.appendFileSync('./PA_CENSUS_3.json', row + ", \n");
        } else if(count < 300000){
            fs.appendFileSync('./PA_CENSUS_4.json', row + ", \n");
        } else {
            fs.appendFileSync('./PA_CENSUS_5.json', row + ", \n");
        }
        count++;
}));