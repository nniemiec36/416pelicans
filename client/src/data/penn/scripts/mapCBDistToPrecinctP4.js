import precincts from './processed/PA_PRECINCTS_P3.json';
import census4 from './processed/broken-up-data/PA_CENSUS_4.json';
import fs from "fs";

for(var i = 0; i<precincts.features.length; i++){
    var pfeat = precincts.features[i].properties;
    var precinctGeoid = pfeat.GEOID20;
    var precFoundC4 = false;
    for(var j = 0; j < census4.features.length; j++){
        var cfeat = census4.features[j].properties;
        var cb_id = cfeat.STATEID + cfeat.COUNTYID + cfeat.VTD;
        //console.log(cb_id);
        //console.log(precinctGeoid);
        if(precinctGeoid === cb_id){
            precFoundC4 = true;
            //console.log("true");
            precincts.features[i].properties.DistrictID=cfeat.DISTRICTID;
            var row = JSON.stringify(precincts.features[i]);
            fs.appendFileSync('./processed/PA_PRECINCTS_P4.json', row + ", \n");
            break;
        }
    }

    if(precFoundC4 === false){
        var row = JSON.stringify(precincts.features[i]);
        fs.appendFileSync('./processed/PA_PRECINCTS_P4.json', row + ", \n");
    }
}

console.log("done");
