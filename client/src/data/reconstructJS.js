// import allInf from '../client/src/data/georgia/processed/GA_PRECINCTS_ALL_INFO.json';
import allInfo from './georgia/processed/GA_PRECINCTS_ALL_INFO.json';
import seawulf from '../../../preprocessing/gerrymandering-mcmc-master/output/recombination_of_districts2.json';
// import seawulf from './gerrymandering-mcmc-master/output/recombination_of_districts2.json';
import fs from 'fs';

var json = {
    type: "FeatureCollection",
    crs: { type: "name", properties: { name: "urn:ogc:def:crs:EPSG::4269" } },
    features:[]
}

for(var i =0;i<seawulf.nodes.length;i++){
    // console.log(seawulf.nodes[i]);
    var jsonRow = {type: "Feature", properties: {}, geometry: {}};
    for(var j =0;j<allInfo.features.length;j++){
        // console.log(allInfo.features[j].properties.PrecinctID);
        if(allInfo.features[i].properties.PrecinctID == seawulf.nodes[j].id){
            var geometry = allInfo.features[i].geometry;
            var info = seawulf.nodes[j];
            jsonRow.properties = info;
            jsonRow.geometry = geometry;
            console.log(jsonRow);
            json.features.push(jsonRow);
            break;
        }
    }
}
fs.writeFileSync('completed.json', JSON.stringify(json), (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});
