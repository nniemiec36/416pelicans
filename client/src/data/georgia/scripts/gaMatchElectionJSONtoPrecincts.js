import precincts from './GA_PRECINCTS_RENUMBER_W_DIST.json';
import electionJSON from '../ga_2020_2020.json';
import fs from "fs"; 

var numberMissed = 0;
for(var i =0;i<precincts.features.length;i++){
    var currPrecinct = precincts.features[i];
    var precinctGEOID = currPrecinct.properties.GEOID20;
    var found = false;
    for( var j = 0; j <electionJSON.features.length;j++ ){
        var electionJSONRow =electionJSON.features[j];
        var electionGEOID = electionJSONRow.GEOID;
        if(precinctGEOID===electionGEOID){
            found = true;
            var PRESREP = electionJSONRow.PRESREP;
            var PRESDEM = electionJSONRow.PRESDEM;
            var PRESTOT =electionJSONRow.PRESTOT;
            var SENATER = electionJSONRow.SENATER;
            var SENATED = electionJSONRow.SENATED;
            var SENATETOT = electionJSONRow.SENATETOT;
            currPrecinct.properties.STATE = currPrecinct.properties.STATEFP20;
            delete currPrecinct.properties.STATEFP20;
            currPrecinct.properties.COUNTY = currPrecinct.properties.COUNTYFP20;
            delete currPrecinct.properties.COUNTYFP20;
            currPrecinct.properties.VTD = currPrecinct.properties.VTDST20;
            delete currPrecinct.properties.VTDST20;
            currPrecinct.properties.GEOID = precinctGEOID;
            delete currPrecinct.properties.GEOID20;
            currPrecinct.properties.NAME = currPrecinct.properties.NAME20;
            delete currPrecinct.properties.NAME20;
            delete currPrecinct.properties.VTDI20;
            delete currPrecinct.properties.NAMELSAD20;
            delete currPrecinct.properties.LSAD20;
            delete currPrecinct.properties.MTFCC20;
            delete currPrecinct.properties.FUNCSTAT20;
            delete currPrecinct.properties.ALAND20;
            delete currPrecinct.properties.AWATER20;
            delete currPrecinct.properties.INTPTLAT20;
            delete currPrecinct.properties.INTPTLON20;
            currPrecinct.properties.PRESREP = PRESREP;
            currPrecinct.properties.PRESDEM = PRESDEM;
            currPrecinct.properties.PRESTOT = PRESTOT;
            currPrecinct.properties.SENATER = SENATER;
            currPrecinct.properties.SENATED = SENATED;
            currPrecinct.properties.SENATETOT = SENATETOT;
            break;
        }
    }
    if(!found){
        numberMissed++;
    }
}
console.log(numberMissed);

fs.writeFile('GA_PRECINCTS_RENUMBER_ALL_INFO.json', JSON.stringify(precincts), (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});




