import precincts from './GA_PRECINCTS_RENUMBER.json';
import fs from "fs";
import JSONStream from 'JSONStream';
import es from "event-stream";

// stream to go over each line in the original census file one by one
var getStream = function () {
    var blockJSONData = '../ga_blocks_20.json',
        stream = fs.createReadStream(blockJSONData, { encoding: 'utf8' }),
        parser = JSONStream.parse('*.*');
    return stream.pipe(parser);
};

getStream()
    .pipe(es.mapSync(function (block) { // for each census block
        var dist = block.properties.CD116;
        var cb_id = "";
        cb_id = cb_id.concat(block.properties.STATEFP20);
        cb_id = cb_id.concat(block.properties.COUNTYFP20);
        cb_id = cb_id.concat(block.properties.VTD);
        var foundPrec = false;
        for(var i = 0; i<precincts.features.length;i++){ // for each precinct
            var pfeat = precincts.features[i].properties;
            var precinctGeoid = pfeat.GEOID20;
            if(precinctGeoid === cb_id){ // this census block is in this precinct
                foundPrec = true;
                var state = block.properties.STATEFP20;
                var county = block.properties.COUNTYFP20;
                var vtd = block.properties.VTD;
                var block = block.properties.BLOCKCE20;
                var precinctid = precincts.features[i].properties.PrecinctID;
                var totalPop =block.properties.P0020001;
                var totalHis = block.properties.P0020002;
                var totalNonHis = block.properties.P0020003;
                var totalAsian = block.properties.P0020008;
                var totalWhite = block.properties.P0020005;
                var totalAA = block.properties.P0020006;
    
                var totalVAP = block.properties.P0040001;
                var totalVAPHis = block.properties.P0040002;
                var totalVAPNonHis = block.properties.P0040003;
                var totalVAPAsian = block.properties.P0040008;
                var totalVAPWhite = block.properties.P0040005;
                var totalVAPAA = block.properties.P0040006;
                addRowToJSON(block, state, county, vtd, precinctid, dist, totalPop, totalHis, totalNonHis, totalAsian, totalWhite, totalAA, 
                    totalVAP, totalVAPHis, totalVAPNonHis, totalVAPAsian, totalVAPWhite, totalVAPAA, block.geometry.coordinates);
                precincts.features[i].properties.DistrictID=dist;
                break;
            }
        }
        if(foundPrec === false)
            console.log(cb_id + " not matched");
    }
));

// remake census block file line by line, only keeping relevant information
function addRowToJSON(block, state, county, vtd, precinct, dist, totalPop, totalHis, totalNonHis, totalAsian, totalWhite, totalAA, totalVAP, totalVAPHis,
    totalVAPNonHis, totalVAPAsian, totalVAPWhite, totalVAPAA, Coords){
    var jsonRow = { type: "Feature", 
                  properties: {"BLOCKID": block, "STATEID": state, "COUNTYID": county, "VTD": vtd, "PRECINCTID": precinct, "DISTRICTID": dist, 
                                "TOTALPOP": totalPop, "TOTALHIS": totalHis, "TOTALNONHIS": totalNonHis, "TOTALASIAN": totalAsian, "TOTALWHITE": totalWhite,
                                "TOTALAA": totalAA, "TOTALVAP": totalVAP, "TOTALVAPHIS": totalVAPHis, "TOTALVAPNONHIS": totalVAPNonHis, 
                                "TOTALVAPASIAN": totalVAPAsian, "TOTALVAPWHITE": totalVAPWhite, "TOTALVAPAA": totalVAPAA},
                  geometry:{
                    type: "Polygon",
                      coordinates: Coords
                  }
                };
    jsonRow = JSON.stringify(jsonRow);
    fs.appendFileSync('./GA_CENSUS_RENUMBER.json', jsonRow + ", \n");
  }