import precincts from '../nc_vtd_2020_bound.json';
import fs from "fs"; 

for(var i =0;i<precincts.features.length;i++){
    precincts.features[i].properties.PrecinctID=""+i+"";
    precincts.features[i].properties.DistrictID="";
    //console.log(precincts.features[i].properties);
}

var precincts2 = JSON.stringify(precincts);

fs.writeFile('NC_PRECINCTS_GEOMETRY2.json', precincts2, (err) => {
    if (err) {
        throw err;
    }
    console.log("JSON data is saved.");
});
