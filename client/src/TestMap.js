import React, { useRef, useEffect, useState, useContext } from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import './TestMap.css';
import Header from './Header';
import Legend from './Legend';
import LeftPanel from './LeftPanel';
import SideTable from './SideTable';
import stateData from './data/statesElections.geojson';
import Constants, {electionRange, districtColors, georgiaMap, northCarolinaMap, pennMap} from './Constants';

mapboxgl.accessToken = 'pk.eyJ1Ijoibm5pZW1pZWMiLCJhIjoiY2t0Z2NlYTBqMGdzYjJ3cGhvNTl5dmk3NSJ9.3OkgWWwVaFcxi77btxn3Zg';

export var testMap;

export default function TestMap(props) {
    testMap = useRef(null);
    const mapContainerRef = useRef(null);
    const [lng, setLng] = useState(-95.7);
    const [lat, setLat] = useState(37.7);
    const [zoom, setZoom] = useState(3);
    let currentState = props.stateName;

    // const [state, setState] = useState({
    //   lng: 5,
    //   lat: 34,
    //   zoom: 2
    // });
    // access via state.lng ?
  
    
    //const [testMap, setTestMap] = useState(null);
  
    // Initialize map when component mounts
    useEffect(() => {
        if(testMap.current) {
          switch(currentState){
            case "ga":
              goToState("Georgia");
              break;
            case "pa":
              goToState("Pennsylvania");
              break;
            case "nc":
              goToState("North Carolina");
              break;
          }
        } else {
          testMap.current = new mapboxgl.Map({
            container: mapContainerRef.current,
            center: [lng, lat],
            zoom: zoom,
            style: 'mapbox://styles/nniemiec/cktg9vbjk177f17uipv8q58j0',
          });
      
          
          
          //var nav = new mapboxgl.NavigationControl();
          //testMap.current.addControl(nav, 'top-right');
          testMap.current.on("load", function() {
            addGeoJSONLayer('none', 'us_states_elections_outline', 'outline', 'waterway-label', 'country', 'election', 'countryOutline');
            addGeoJSONLayer('none', 'us_states_elections', 'fill', 'us_states_elections_outline', 'country', 'election', 'countryFill');
            var nav = new mapboxgl.NavigationControl();
            testMap.current.addControl(nav, 'top-right');
            
          });
        }
      //setTestMap(testMap);
    });

    useEffect(() => { // map can move on click
      if (!testMap.current) return; // wait for map to initialize
      testMap.current.on('click', 'us_states_elections', function (e) {
        var stateName = e.features[0].properties.State;
        goToState(stateName);
      });
    });

    function goToState(state){
      switch(state){
        case "Georgia":
          setLat(georgiaMap.lat);
          setLng(georgiaMap.lng);
        break;
        case "North Carolina":
          setLat(northCarolinaMap.lat);
          setLng(northCarolinaMap.lng);
        break;
        case "Pennsylvania" :
          setLat(pennMap.lat);
          setLng(pennMap.lng);
        break;
      }
      setZoom(6);
      testMap.current.flyTo({
        center: [lng, lat],
        zoom: zoom
      });
    }

    function addGeoJSONLayer(state, layerID, fillType, aboveLayer, level, mapView, boundaryType, visibility) {
      let paintVar = '';
      setLat( 41.203323);
      setLng(-76.694527);
      setZoom(6);
      testMap.current.flyTo({ // not working
        center: [lng, lat],
        zoom: zoom
      });
      

      if(fillType = 'fill'){
        if(level === 'country')
          paintVar = electionRange;
      } else {
        if(level === 'country')
          paintVar = {
            "line-color": "#ffffff",
            "line-width": 0.8,
          };
      }
      testMap.current.addLayer({
        "id": layerID,
        "type": fillType,
        source: {
          type: "geojson",
          data: stateData,
        },
        layout: {
          'visibility': visibility,
        },
        paint: paintVar,
      },
      aboveLayer
      );
    }

    return (
      <div>
        <div ref={mapContainerRef} className='map-container'  />
        
        <Header/>
        <p>TEST TEXT {lng}</p> 
        <Legend/>
        <SideTable/>
        <LeftPanel/>
      </div>
    );
  };

  //export default TestMap;