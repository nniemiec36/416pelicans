export const electionRange = {
    "fill-color": [
      "match",
      ["get", "Winner"],
      "Donald J Trump",
      "#cf635d",
      "Joseph R Biden Jr",
      "#6193c7",
      "Other",
      "#91b66e",
      "#eff1f1",
    ],
    "fill-outline-color": "#ffffff",
    "fill-opacity": [
        "step",
        ["get", "WnrPerc"],
        0.3,
        0.4,
        0.5,
        0.5,
        0.7,
        0.6,
        0.9,
    ],
  };
export const congressElection = {
  "fill-color": [
    "match",
    ["get", "WINNER"],
    "R",
    "#cf635d",
    "D",
    "#6193c7",
    "#eff1f1",
  ],
  "fill-outline-color": "#000000",
};

export const districtColors = {
    "fill-color": [
      "match",
      ["get", "CD116FP"],
      "01", "#bfff00", //green
      "18", "#8258fa", //purple
      "07", "#faac58", // orange
      "02", "#f7819f", // red
      "17", "#f2f5a9", // yellow green
      "14", "#045fb4", // dark blue
      "04", "#a9f5d0", // seafoam green
      "03", "#5f04b4", // dark purple
      "10", "#f6cef5", // light pink
      "06", "#cee3f6", // light blue
      "05", "#088a08", // dark green
      "16", "#df013a", // red
      "13", "#df01a5", // magenta
      "11", "#00bfff", // sky blue
      "09", "#e3cef6", // lilac
      "15", "#ffff00", // yellow,
      "12", "#848484", // grey
      "08", "#5858fa", // purple blue
      "19", "#fcba03", // kinda orange
      "20", "#59bfde", // sorta blue
      "21", "#5971de", // sorta indigo
      "22", "#dede59", // mustard yellow
      "23", "#de3e3e", // sorta red
      "24", "#d559de", //sorta pink purple
      "25", "#dae8a9", // sage green 
      "26", "#a9e8d6", // sorta teal
      "#eff1f1",
    ],
    "fill-outline-color": "#000000",
    "fill-opacity": 0.6,
  };

  export const redistrColors = {
    "fill-color": [
      "match",
      ["get", "DISTRICT"],
      "1", "#bfff00", //green
      "18", "#8258fa", //purple
      "7", "#faac58", // orange
      "2", "#f7819f", // red
      "17", "#f2f5a9", // yellow green
      "14", "#045fb4", // dark blue
      "4", "#a9f5d0", // seafoam green
      "3", "#5f04b4", // dark purple
      "10", "#f6cef5", // light pink
      "6", "#cee3f6", // light blue
      "5", "#088a08", // dark green
      "16", "#df013a", // red
      "13", "#df01a5", // magenta
      "11", "#00bfff", // sky blue
      "9", "#e3cef6", // lilac
      "15", "#ffff00", // yellow,
      "12", "#848484", // grey
      "8", "#5858fa", // purple blue
      "19", "#fcba03", // kinda orange
      "20", "#59bfde", // sorta blue
      "21", "#5971de", // sorta indigo
      "22", "#dede59", // mustard yellow
      "23", "#de3e3e", // sorta red
      "24", "#d559de", //sorta pink purple
      "25", "#dae8a9", // sage green 
      "26", "#a9e8d6", // sorta teal
      "#eff1f1",
    ],
    "fill-outline-color": "#000000",
    "fill-opacity": 0.6,
  };


  export const georgiaMap = {
    "lng": -82.441162,
    "lat": 32.647875
  }

  export const northCarolinaMap = {
    "lng": -78.793457,
    "lat": 35.782169
  }

  export const pennMap = {
    "lng": -76.694527,
    "lat": 41.203323
  }

  const lineLayers = ['District','County','Precinct'];
  const lineColors = ['#4bde72','#e8e8e8','#000000'];

  export const precinctLines = {
    "line-color": "#444444",
    "line-width": 0.30,
  };

  export const countyLines = {
    "line-color": "#ffffff",
    "line-width": 0.20,
  };

  export const districtLines = {
    "line-color": "#00ff00",
    "line-width": 0.90,
  };

  export const stateLines = {
    "line-color": "#ffffff",
    "line-width": 0.8,
  };

  export const pennElectionLayers = ['pa_counties_elections_outline', 'pa_counties_elections', 'pa_precincts', 'pa_congressional_districts'];
  export const pennDistrictLayers = ['pa_counties_elections_outline', 'pa_precincts', 'pa_congressional_districts', 'pa_original_districts'];
  export const pennCongElectLayers = ['pa_counties_elections_outline', 'pa_precincts', 'pa_congressional_districts', 'pa_congress_2020'];

  export const georgiaElectionLayers = ['ga_counties_elections','ga_counties_elections_outline','ga_precincts','ga_congressional_districts'];
  export const georgiaDistrictLayers =['ga_counties_elections_outline', 'ga_precincts', 'ga_congressional_districts', 'ga_original_districts'];
  export const georgiaCongElectLayers = ['ga_counties_elections_outline','ga_precincts','ga_congressional_districts','ga_congress_2020'];

  export const northCarolinaElectionLayers = ['nc_counties_elections_outline', 'nc_counties_elections', 'nc_precincts_18', 'nc_congressional_districts'];
  export const northCarolinaDistrictLayers = ['nc_counties_elections_outline', 'nc_precincts', 'nc_congressional_districts', 'nc_original_districts'];
  export const northCarolinaCongElectLayers = ['nc_counties_elections_outline', 'nc_precincts', 'nc_congressional_districts', 'nc_congress_2020'];



