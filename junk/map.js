mapboxgl.accessToken = 'pk.eyJ1Ijoibm5pZW1pZWMiLCJhIjoiY2t0Z2NlYTBqMGdzYjJ3cGhvNTl5dmk3NSJ9.3OkgWWwVaFcxi77btxn3Zg';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/nniemiec/cktg9vbjk177f17uipv8q58j0',
    zoom: 3,
    maxZoom: 12,
    minZoom: 3,
    center: [-95.7, 37.7]
});

var nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'top-right');

var currentState = 'none';
var districtView = true;
var chosenPlan = 'none';

map.on("load", function () {
    map.addLayer( //State Outline
      {
        id: "us_states_elections_outline",
        type: "line",
        source: {
          type: "geojson",
          data: "data/statesElections.geojson",
        },
        maxzoom: 6,
        paint: {
          "line-color": "#ffffff",
          "line-width": 0.7,
        },
      },
      "waterway-label" // Here's where we tell Mapbox where to slot this new layer
    ); 
    map.addLayer( //State Elections
      {
        id: "us_states_elections",
        type: "fill",
        source: {
          type: "geojson",
          data: "data/statesElections.geojson",
        },
        maxzoom: 6,
        paint: {
          "fill-color": [
            "match",
            ["get", "Winner"],
            "Donald J Trump",
            "#cf635d",
            "Joseph R Biden Jr",
            "#6193c7",
            "Other",
            "#91b66e",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": [
              "step",
              ["get", "WnrPerc"],
              0.3,
              0.4,
              0.5,
              0.5,
              0.7,
              0.6,
              0.9,
          ],
        },
      },
      "us_states_elections_outline" // Here's where we tell Mapbox where to slot this new layer
    );

    map.addLayer(// Georgia's Counties Outline
        {
          id: "ga_counties_elections_outline",
          type: "line",
          source: {
            type: "geojson",
            data: "data/georgia/georgias-counties.geojson",
          },
          layout: {
            'visibility': 'none'
            },
          minzoom: 6,
          paint: {
            "line-color": "#ffffff",
            "line-width": 0.25,
          },
        },
        "us_states_elections"
      );
    map.addLayer(// Georgia's Counties Elections
    {
      id: "ga_counties_elections",
      type: "fill",
      source: {
        type: "geojson",
        data: "data/georgia/georgias-counties.geojson",
      },
      layout: {
        'visibility': 'none'
      },
      minzoom: 6,
      paint: {
        "fill-color": [
          "match",
          ["get", "Winner"],
          "Donald J Trump",
          "#cf635d",
          "Joseph R Biden Jr",
          "#6193c7",
          "Other",
          "#91b66e",
          "#eff1f1",
        ],
        "fill-outline-color": "#000000",
        "fill-opacity": [
          "step",
          ["get", "WnrPerc"],
          0.3,
          0.4,
          0.5,
          0.5,
          0.7,
          0.6,
          0.9,
        ],
      },
    },
    "ga_counties_elections_outline"
  );
    // North Carolina's Counties and Elections Mapping
    map.addLayer( // North Carolina's Counties Outline
      {
        id: "nc_counties_elections_outline",
        type: "line",
        source: {
          type: "geojson",
          data: "data/north-carolina/nc-counties.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "line-color": "#ffffff",
          "line-width": 0.25,
        },
      },
      "us_states_elections"
    );
    map.addLayer( // NC Counties Elections
      {
        id: "nc_counties_elections",
        type: "fill",
        source: {
          type: "geojson",
          data: "data/north-carolina/nc-counties.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "fill-color": [
            "match",
            ["get", "Winner"],
            "Donald J Trump",
            "#cf635d",
            "Joseph R Biden Jr",
            "#6193c7",
            "Other",
            "#91b66e",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": [
            "step",
            ["get", "WnrPerc"],
            0.3,
            0.4,
            0.5,
            0.5,
            0.7,
            0.6,
            0.9,
          ],
        },
      },
      "nc_counties_elections_outline"
    );
    // Pennsylvania's Counties and Elections Mapping
    map.addLayer( // PA Counties Outline
      {
        id: "pa_counties_elections_outline",
        type: "line",
        source: {
          type: "geojson",
          data: "data/penn/pa-counties.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "line-color": "#ffffff",
          "line-width": 0.25,
        },
      },
      "us_states_elections"
    );
    map.addLayer( // PA Counties Elections
      {
        id: "pa_counties_elections",
        type: "fill",
        source: {
          type: "geojson",
          data: "data/penn/pa-counties.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "fill-color": [
            "match",
            ["get", "Winner"],
            "Donald J Trump",
            "#cf635d",
            "Joseph R Biden Jr",
            "#6193c7",
            "Other",
            "#91b66e",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": [
            "step",
            ["get", "WnrPerc"],
            0.3,
            0.4,
            0.5,
            0.5,
            0.7,
            0.6,
            0.9,
          ],
        },
      },
      "pa_counties_elections_outline"
    );
    map.addLayer( // GA Precincts
        {
            id: "ga_precincts",
            type: "line",
            source: {
            type: "geojson",
            data: "data/georgia/ga_precinct_2020.geojson",
          },
          layout: {
            'visibility': 'none'
            },
            minzoom: 6,
            paint: {
              "line-color": "#444444",
              "line-width": 0.30,
            }, 
        },
        "ga_counties_elections_outline"
    );
    map.addLayer( // PA Precincts
      {
        id: "pa_precincts_18",
        type: "line",
        source: {
          type: "geojson",
          data: "data/penn/pa_precincts_18.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "line-color": "#444444",
          "line-width": 0.30,
        },
      },
      "pa_counties_elections_outline"
    );
    map.addLayer( // NC Precincts
      {
        id: "nc_precincts",
        type: "line",
        source: {
          type: "geojson",
          data: "data/north-carolina/nc_2016.geojson",
        },
        layout: {
          'visibility': 'none'
          },
        minzoom: 6,
        paint: {
          "line-color": "#444444",
          "line-width": 0.30,
        },
      },
      "nc_counties_elections_outline"
    );
    map.addLayer( // GA CD
      {
        id: "ga_congressional_districts",
        type: "line",
        source: {
          type: "geojson",
          data: "data/georgia/ga_cd.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "ga_counties_elections_outline"
    );
    map.addLayer( // NC CD
      {
        id: "nc_congressional_districts",
        type: "line",
        source: {
          type: "geojson",
          data: "data/north-carolina/nc_cd.geojson",
        },
        layout: {
          'visibility': 'none'
        },
        minzoom: 6,
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.9,
        },
      },
      "nc_counties_elections_outline"
    );
    map.addLayer( // PA CD
      {
        id: "pa_congressional_districts",
        type: "line",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/penn/pa_cd.geojson",
        },
        minzoom: 6,
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.9,
        },
      },
      "pa_counties_elections_outline"
    );
    map.addLayer( // PA Original CD
      {
        id: "pa_original_districts",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/penn/pa_cd.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "CD116FP"],
            "01", "#bfff00", //green
            "18", "#8258fa", //purple
            "07", "#faac58", // orange
            "02", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "04", "#a9f5d0", // seafoam green
            "03", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "06", "#cee3f6", // light blue
            "05", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "09", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "08", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "pa_congressional_districts"
    );
    map.addLayer( // PA Congressional Election
      {
        id: "pa_congress_2020",
        type: "fill",
        layout: {
          'visibility':'none'
        },
        source: {
          type: "geojson",
          data: "data/penn/pa_cd.geojson"
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "WINNER"],
            "R",
            "#cf635d",
            "D",
            "#6193c7",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
        },
        minzoom: 6,
      },
      "pa_congressional_districts"
    );
    map.addLayer( // GA Original CD
      {
        id: "ga_original_districts",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/georgia/ga_cd.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "CD116FP"],
            "01", "#bfff00", //green
            "18", "#8258fa", //purple
            "07", "#faac58", // orange
            "02", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "04", "#a9f5d0", // seafoam green
            "03", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "06", "#cee3f6", // light blue
            "05", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "09", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "12", "#848484", // grey
            "08", "#5858fa", // purple blue
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "ga_congressional_districts"
    );
    map.addLayer( // GA Congressional Election
      {
        id: "ga_congress_2020",
        type: "fill",
        layout: {
          'visibility':'none'
        },
        source: {
          type: "geojson",
          data: "data/georgia/ga_cd.geojson"
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "WINNER"],
            "R",
            "#cf635d",
            "D",
            "#6193c7",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
        },
        minzoom: 6,
      },
      "ga_congressional_districts"
    );
    map.addLayer( // NC Original CD
      {
        id: "nc_original_districts",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/north-carolina/nc_cd.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "CD116FP"],
            "01", "#bfff00", //green
            "18", "#8258fa", //purple
            "07", "#faac58", // orange
            "02", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "04", "#a9f5d0", // seafoam green
            "03", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "06", "#cee3f6", // light blue
            "05", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "09", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "12", "#848484", // grey
            "08", "#5858fa", // purple blue
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "nc_congressional_districts"
    );
    map.addLayer( // NC Congressional Election
      {
        id: "nc_congress_2020",
        type: "fill",
        layout: {
          'visibility':'none'
        },
        source: {
          type: "geojson",
          data: "data/north-carolina/nc_cd.geojson"
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "WINNER"],
            "R",
            "#cf635d",
            "D",
            "#6193c7",
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
        },
        minzoom: 6,
      },
      "nc_congressional_districts"
    );
    map.addLayer( // GA Redistricting 1 Outlines
      {
        id: "ga_redistricting_1",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr1.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "ga_counties_elections_outline"
    );

    map.addLayer( // GA Redistricting 1 Fill
      {
        id: "ga_redistricting_1_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr1.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "ga_redistricting_1"
    );
    map.addLayer( // GA Redistricting 2 Outlines
      {
        id: "ga_redistricting_2",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr2.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "ga_counties_elections_outline"
    );

    map.addLayer( // GA Redistricting 2 Fill
      {
        id: "ga_redistricting_2_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr2.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "8", "#5858fa", // purple blue
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "ga_redistricting_2"
    );
    map.addLayer( // GA Redistricting 3 Outlines
      {
        id: "ga_redistricting_3",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr3.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "ga_counties_elections_outline"
    );

    map.addLayer( // GA Redistricting 3 Fill
      {
        id: "ga_redistricting_3_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/georgia/redistr3.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "ga_redistricting_3"
    );

    map.addLayer( // PA Redistricting 1 Outlines
    {
      id: "pa_redistricting_1",
      type: "line",
      source: {
        type: "geojson",
        data: "data/redistrictings/penn/redistr1.geojson",
      },
      minzoom: 6,
      layout: {
        'visibility': 'none'
        },
      paint: {
        "line-color": "#00ff00",
        "line-width": 0.90,
      },
    },
    "pa_counties_elections_outline"
    );

    map.addLayer( // PA Redistricting 1 Fill
      {
        id: "pa_redistricting_1_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/penn/redistr1.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "pa_redistricting_1"
    );
    map.addLayer( // PA Redistricting 2 Outlines
      {
        id: "pa_redistricting_2",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/penn/redistr2.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "pa_counties_elections_outline"
    );

    map.addLayer( // PA Redistricting 2 Fill
      {
        id: "pa_redistricting_2_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/penn/redistr2.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "pa_redistricting_2"
    );
    map.addLayer( // PA Redistricting 3 Outlines
      {
        id: "pa_redistricting_3",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/penn/redistr3.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "pa_counties_elections_outline"
    );

    map.addLayer( // PA Redistricting 3 Fill
      {
        id: "pa_redistricting_3_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/penn/redistr3.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "pa_redistricting_3"
    );

    map.addLayer( // NC Redistricting 1 Outlines
      {
        id: "nc_redistricting_1",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr1.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "nc_counties_elections_outline"
    );

    map.addLayer( // NC Redistricting 1 Fill
      {
        id: "nc_redistricting_1_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr1.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "nc_redistricting_1"
    );
    map.addLayer( // NC Redistricting 2 Outlines
      {
        id: "nc_redistricting_2",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr2.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "nc_counties_elections_outline"
    );

    map.addLayer( // NC Redistricting 2 Fill
      {
        id: "nc_redistricting_2_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr2.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "nc_redistricting_2"
    );
    map.addLayer( // NC Redistricting 3 Outlines
      {
        id: "nc_redistricting_3",
        type: "line",
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr3.geojson",
        },
        minzoom: 6,
        layout: {
          'visibility': 'none'
          },
        paint: {
          "line-color": "#00ff00",
          "line-width": 0.90,
        },
      },
      "nc_counties_elections_outline"
    );

    map.addLayer( // NC Redistricting 3 Fill
      {
        id: "nc_redistricting_3_map",
        type: "fill",
        layout: {
          'visibility': 'none'
          },
        source: {
          type: "geojson",
          data: "data/redistrictings/north-carolina/redistr3.geojson",
        },
        paint: {
          "fill-color": [
            "match",
            ["get", "DISTRICT"],
            "1", "#bfff00", //green
            "18", "#8258fa", //purple
            "7", "#faac58", // orange
            "2", "#f7819f", // red
            "17", "#f2f5a9", // yellow green
            "14", "#045fb4", // dark blue
            "4", "#a9f5d0", // seafoam green
            "3", "#5f04b4", // dark purple
            "10", "#f6cef5", // light pink
            "6", "#cee3f6", // light blue
            "5", "#088a08", // dark green
            "16", "#df013a", // red
            "13", "#df01a5", // magenta
            "11", "#00bfff", // sky blue
            "9", "#e3cef6", // lilac
            "15", "#ffff00", // yellow,
            "12", "#848484", // grey
            "8", "#5858fa", // purple blue
            "19", "#fcba03", // kinda orange
            "20", "#59bfde", // sorta blue
            "21", "#5971de", // sorta indigo
            "22", "#dede59", // mustard yellow
            "23", "#de3e3e", // sorta red
            "24", "#d559de", //sorta pink purple
            "25", "#dae8a9", // sage green 
            "26", "#a9e8d6", // sorta teal
            "#eff1f1",
          ],
          "fill-outline-color": "#000000",
          "fill-opacity": 0.6,
        },
        minzoom: 6,
      },
      "nc_redistricting_3"
    );
    
    const legend = document.getElementById('legend');
    const label = document.createElement('label');
    label.innerHTML = `Legend`;
    legend.appendChild(label);
    districtLegend("none");
});


function politicalLegend(){
  
  lineLegend();
  const layers = [
    'Democratic',
    'Republican'
  ];
  const colors = [
    '#6193c7',
    '#cf635d'
  ];
  const legend = document.getElementById('legend');

  layers.forEach((layer, i) => {
    const color = colors[i];
    const item = document.createElement('div');
    item.className = "legend-item";
    const key = document.createElement('span');
    key.className = 'legend-key';
    key.style.backgroundColor = color;

    const value = document.createElement('span');
    value.innerHTML = `${layer}`;
    item.appendChild(key);
    item.appendChild(value);
    legend.appendChild(item);
  });
}

function clearLegend(){
  var elements=document.getElementsByClassName('legend-item');
  while(elements.length > 0){
    elements[0].parentNode.removeChild(elements[0]);
  }
}

function districtLegend(state){
  lineLegend();
  console.log("here");
  var num;
  if(state == 'ga'){
    num = 14;
  }else if (state=='nc'){
    num = 13
  }else if (state=='pa'){
    num = 25
  }else{
    num=0;
  }
  const layers = [
    'District 1',
    'District 2',
    'District 3',
    'District 4',
    'District 5',
    'District 6',
    'District 7',
    'District 8',
    'District 9',
    'District 10',
    'District 11',
    'District 12',
    'District 13',
    'District 14',
    'District 15',
    'District 16',
    'District 17',
    'District 18',
    'District 19',
    'District 20',
    'District 21',
    'District 22',
    'District 23',
    'District 24',
    'District 25'
  ];
  const colors = [
    "#bfff00", //green
    "#8258fa", //purple
    "#faac58", // orange
    "#f7819f", // red
    "#f2f5a9", // yellow green
    "#045fb4", // dark blue
    "#a9f5d0", // seafoam green
    "#5f04b4", // dark purple
    "#f6cef5", // light pink
    "#cee3f6", // light blue
    "#088a08", // dark green
    "#df013a", // red
    "#df01a5", // magenta
    "#00bfff", // sky blue
    "#e3cef6", // lilac
    "#ffff00", // yellow,
    "#fcba03", // kinda orange
    "#59bfde", // sorta blue
    "#5971de", // sorta indigo
    "#dede59", // mustard yellow
    "#de3e3e", // sorta red
    "#d559de", //sorta pink purple
    "#dae8a9", // sage green 
    "#a9e8d6", // sorta teal
    "#848484" // grey
  ];
  const legend = document.getElementById('legend');

  layers.forEach((layer, i) => {
    if(i>=num)
      return;
    const color = colors[i];
    const item = document.createElement('div');
    item.className = "legend-item";
    const key = document.createElement('span');
    key.className = 'legend-key';
    key.style.backgroundColor = color;

    const value = document.createElement('span');
    value.innerHTML = `${layer}`;
    item.appendChild(key);
    item.appendChild(value);
    legend.appendChild(item);
  });
}

function lineLegend(){
  const layers = [
    'District',
    'County',
    'Precinct'
  ];
  const colors = [
    '#4bde72',
    '#e8e8e8',
    '#000000'
  ];
  const legend = document.getElementById('legend');

  layers.forEach((layer, i) => {
    const color = colors[i];
    const item = document.createElement('div');
    item.className = "legend-item";
    const key = document.createElement('span');
    key.className = 'legend-key';
    key.style.height = '2px';
    key.style.backgroundColor = color;

    const value = document.createElement('span');
    value.innerHTML = `${layer}`;
    item.appendChild(key);
    item.appendChild(value);
    legend.appendChild(item);
  });
}

map.on('click', 'us_states_elections', function (e) {
    var stateName = e.features[0].properties.State;
    if(stateName === 'Georgia'){
      currentState = 'ga';
        map.flyTo({
            center: [-82.441162, 32.647875],
            zoom: 6
        });

        

        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          georgiaDistrictMap();
        } else if(mapOption === 'congress-results'){
          georgiaConDisMap();
        } else {
          georgiaElectionMap();
        }
        openTable();

    } else if(stateName === 'North Carolina'){
      currentState = 'nc';
        map.flyTo({
            center: [-78.793457, 35.782169],
            zoom: 6
        });
        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          northCarolinaDistrictMap();
        } else if(mapOption === 'congress-results'){
          northCarolinaConDisMap();
        } else {
          northCarolinaElectionMap();
      }

      document.getElementById("table").style.background="rgba(255, 255, 255, 0.3)";
      openTable();

    } else if(stateName === 'Pennsylvania'){
      currentState = 'pa';
        map.flyTo({
            center: [-76.694527, 41.203323],
            zoom: 6
        });
        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          pennDistrictMap();
        } else if(mapOption === 'congress-results'){
          pennConDisMap();
        } else {
          pennElectionMap();
      }

      openTable();
    } else { // resets map
      currentState = 'none';
        map.flyTo({
            center: [-85.5, 37.7],
            zoom: 3 
        })
        closeTable();
    }
})

map.on('click', 'us_counties_elections', function (e) {
  var stateName = e.features[0].properties.State;
  if(stateName !== 'Pennsylvania' || stateName !== 'North Carolina'
    || stateName !== 'Georgia'){
        map.flyTo({
          center: [-85.5, 37.7],
          zoom: 3 
      });
      const boundaries = ['pa_counties_elections',
      'ga_counties_elections',
      'nc_counties_elections',
      'ga_counties_elections_outline',
      'nc_counties_elections_outline', 
      'pa_counties_elections_outline', 
      'pa_precincts_18',
      'ga_precincts',
      'nc_precincts',
      'ga_congressional_districts', 
      'pa_congressional_districts',
      'nc_congressional_districts'];
      for (const id of boundaries){
        map.setLayoutProperty(
          id,
          'visibility',
          'none'
        );
      } 
    }

});

// Change the cursor to a pointer when the mouse is over the us_states_elections layer.
map.on('mouseenter', 'us_states_elections', function () {
    map.getCanvas().style.cursor = 'pointer';
});
// Change it back to a pointer when it leaves.
map.on('mouseleave', 'us_states_elections', function () {
    map.getCanvas().style.cursor = '';
});


/* Refresh */
map.on('load', () => {  
  const generate = document.getElementById('generate');
  generate.onclick = function() {
    if(currentState=="ga"){
      const off = ['ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map'];
      for (const id of off){
        map.setLayoutProperty(id, 'visibility', 'none');
      }
      const on = ['ga_congressional_districts', 'ga_original_districts'];
      for(const id of on){
        map.setLayoutProperty(id, 'visibility', 'visible');
      }
    }else if (currentState=='pa'){
      const off = ['pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map'];
      for (const id of off){
        map.setLayoutProperty(id, 'visibility', 'none');
      }
      const on = ['pa_congressional_districts', 'pa_original_districts'];
      for(const id of on){
        map.setLayoutProperty(id, 'visibility', 'visible');
      }
    }else if ( currentState=='nc'){
      const off = ['nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map'];
      for (const id of off){
        map.setLayoutProperty(id, 'visibility', 'none');
      }
      const on = ['nc_congressional_districts', 'nc_original_districts'];
      for(const id of on){
        map.setLayoutProperty(id, 'visibility', 'visible');
      }
    }else{
      //error
    }
    fillComparisonTable();
  }
  

  const refresh = document.getElementById('reset-map');
  refresh.onclick = function() {
    clearLegend();
    lineLegend();
    currentState = 'none';
        map.flyTo({
            center: [-85.5, 37.7],
            zoom: 3 
        })
        closeTable();
  }
  
  const checkBox = document.getElementById('show-district-checkbox');
  checkBox.onclick = function () {
    districtView = checkBox.checked;
    console.log(districtView);
    // Enumerate ids of the layers.
    const toggleableLayerIds = ['ga_congressional_districts', 'pa_congressional_districts', 'nc_congressional_districts'];
    // Set up the corresponding toggle button for each layer.
    if (districtView == false) {
      for (const id of toggleableLayerIds) {
        console.log(id);
        map.setLayoutProperty(id, 'visibility', 'none');
        this.className = '';
      }
    } else if (districtView == true){
      if(currentState == 'ga'){
        this.className = 'active';
        map.setLayoutProperty(toggleableLayerIds[0],'visibility','visible');
      } else if(currentState == 'nc'){
        this.className = 'active';
        map.setLayoutProperty(toggleableLayerIds[2],'visibility','visible');
      } else if(currentState == 'pa'){
        this.className = 'active';
        map.setLayoutProperty(toggleableLayerIds[1],'visibility','visible');
      } else {
        for (const id of toggleableLayerIds) {
          console.log(id);
          map.setLayoutProperty(id, 'visibility', 'none');
          this.className = '';
        }
      }
      
    }      
  }
});

/* Map Drop Down Controls */
map.on('load', () => {
  const mapDropDown = document.getElementById('map-view-options');
  mapDropDown.onchange = function () {
    const stateDropDown = document.getElementById('state-dropdown-options');
    var mapView = mapDropDown.value;
    console.log(mapView);
    if(mapView === 'election-results'){
      if(currentState === 'ga'){
        georgiaElectionMap();
      } else if(currentState === 'nc'){
        northCarolinaElectionMap();
      } else if(currentState === 'pa'){
        pennElectionMap();
      } else {
        //nothing
      }
      clearLegend();
      politicalLegend();
    } else if(mapView === 'congress-results'){
        if(currentState === 'ga'){
          georgiaConDisMap();
        } else if(currentState === 'nc'){
          northCarolinaConDisMap();
        } else if(currentState === 'pa'){
          pennConDisMap();
        } else {
          //nothing
        }
        clearLegend();
        politicalLegend();
    } else {
      clearLegend();
      if(currentState === 'ga'){
        georgiaDistrictMap();
      } else if(currentState === 'nc'){
        northCarolinaDistrictMap();
      } else if(currentState === 'pa'){
        pennDistrictMap();
      } else {
        districtLegend('none');
        //nothing
      }
      console.log("here 2");
      
    }
  }
})

/* State Drop Down Controls */
map.on('load', () => {   
  const stateDropDown = document.getElementById('state-dropdown-options');
  stateDropDown.onchange = function () {
    var stateName = stateDropDown.value;
    console.log(stateName);
    if(stateName === 'ga-jump'){
      currentState = 'ga';
        map.flyTo({
            center: [-82.441162, 32.647875],
            zoom: 6
        });

        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          georgiaDistrictMap();
        } else if(mapOption === 'congress-results'){
          georgiaConDisMap();
        } else {
          georgiaElectionMap();
        }
        openTable();

    } else if(stateName === 'nc-jump'){
      currentState = 'nc';
        map.flyTo({
            center: [-78.793457, 35.782169],
            zoom: 6
        });

        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          northCarolinaDistrictMap();
        } else if(mapOption === 'congress-results'){
          northCarolinaConDisMap();
        }else {
          northCarolinaElectionMap();
        }
        openTable();

    } else if(stateName === 'pa-jump'){
      currentState = 'pa';
        map.flyTo({
            center: [-76.694527, 41.203323],
            zoom: 6
        });
        
        const mapView = document.getElementById('map-view-options');
        const mapOption = mapView.value;
        if(mapOption === 'district-plans'){
          pennDistrictMap();
        } else if(mapOption === 'congress-results'){
          pennConDisMap();
        } else {
          pennElectionMap();
        }

        openTable();

    } else { // resets map
      
      currentState = 'none';
        map.flyTo({
            center: [-85.5, 37.7],
            zoom: 3 
        })
        closeTable();
    }
    
  }
});

function georgiaElectionMap() {
  const georgia = ['ga_counties_elections', 
    'ga_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts'];

  const others = ['nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of georgia){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'ga_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function georgiaDistrictMap() {
  clearLegend();
  districtLegend('ga');
  const georgia = ['ga_counties_elections_outline',
    'ga_precincts',
    'ga_congressional_districts',
    'ga_original_districts'];

  const others = ['ga_counties_elections',
    'nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'nc_original_districts',
    'pa_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of georgia){
    map.setLayoutProperty(
      id,'visibility', 'visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'ga_congressional_districts',
      'visibility',
      'none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,
      'visibility',
      'none'
    );
  }
}

function georgiaConDisMap() {
  const georgia = ['ga_counties_elections', 
    'ga_counties_elections_outline', 
    'ga_precincts', 
    'ga_congress_2020', 'ga_congressional_districts'];

  const others = ['nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of georgia){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'ga_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function northCarolinaDistrictMap() {
  clearLegend();
  districtLegend('nc');
  const nc = ['nc_counties_elections_outline',
    'nc_precincts',
    'nc_congressional_districts',
    'nc_original_districts'];

  const others = ['nc_counties_elections',
    'ga_counties_elections', 
    'ga_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'ga_original_districts',
    'pa_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of nc){
    map.setLayoutProperty(
      id,'visibility', 'visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'nc_congressional_districts',
      'visibility',
      'none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,
      'visibility',
      'none'
    );
  }

}

function northCarolinaElectionMap() {
  const nc = ['nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts'];

  const others = ['ga_counties_elections', 
    'ga_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts',
    'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of nc){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'nc_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function northCarolinaConDisMap() {
  const nc = ['nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congress_2020', 'nc_congressional_districts'];

  const others = ['ga_counties_elections', 
    'ga_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts',
    'pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts',
    'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'pa_congress_2020'];

  for (const id of nc){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'nc_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function pennElectionMap() {
  const penn = ['pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 
    'pa_congressional_districts'];

  const others = ['ga_counties_elections', 
    'pa_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts',
    'nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

  for (const id of penn){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'pa_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function pennDistrictMap() {
  clearLegend();
  districtLegend('pa');
  const penn = ['pa_counties_elections_outline',
  'pa_precincts_18',
  'pa_congressional_districts',
  'pa_original_districts'];

const others = ['pa_counties_elections',
  'ga_counties_elections', 
  'ga_counties_elections_outline', 
  'ga_precincts', 
  'ga_congressional_districts',
  'nc_counties_elections', 
  'nc_counties_elections_outline', 
  'nc_precincts', 
  'nc_congressional_districts',
  'ga_original_districts',
  'nc_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020', 'pa_congress_2020'];

for (const id of penn){
  map.setLayoutProperty(
    id,'visibility', 'visible'
  );
}

if(districtView==false){
  map.setLayoutProperty(
    'pa_congressional_districts',
    'visibility',
    'none'
  );
}

for (const id of others){
  map.setLayoutProperty(
    id,
    'visibility',
    'none'
  );
}
}

function pennConDisMap() {
  const penn = ['pa_counties_elections', 
    'pa_counties_elections_outline', 
    'pa_precincts_18', 'pa_congressional_districts', 
    'pa_congress_2020'];

  const others = ['ga_counties_elections', 
    'pa_counties_elections_outline', 
    'ga_precincts', 
    'ga_congressional_districts',
    'nc_counties_elections', 
    'nc_counties_elections_outline', 
    'nc_precincts', 
    'nc_congressional_districts',
    'pa_original_districts',
    'nc_original_districts',
    'ga_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map', 'ga_congress_2020', 'nc_congress_2020'];

  for (const id of penn){
    map.setLayoutProperty(
      id,'visibility','visible'
    );
  }

  if(districtView==false){
    map.setLayoutProperty(
      'pa_congressional_districts','visibility','none'
    );
  }

  for (const id of others){
    map.setLayoutProperty(
      id,'visibility','none'
    );
  }
}

function openTable() {
  document.getElementById("table").style.background="rgba(255, 255, 255, 0.3)";
  document.getElementById('tableLinks').style.display = 'block';
  document.getElementById('State').style.display = 'block';
  console.log("table opened");
  fillTable();
}

function closeTable() {
  document.getElementById("table").style.background="rgba(255, 255, 255, 0)";
  document.getElementById('tableLinks').style.display = 'none';
  document.getElementById('State').style.display = 'none';
  document.getElementById('District').style.display = 'none';
  document.getElementById('Comparison').style.display = 'none';
}

function openStateTable(){
  console.log("open state");
    document.getElementById('State').style.display = 'block';
    document.getElementById('District').style.display = 'none';
    document.getElementById('Comparison').style.display = 'none';
}

function openComparisonTable(){
  console.log("open comparison");
    document.getElementById('Comparison').style.display = 'block';
    document.getElementById('State').style.display = 'none';
    document.getElementById('District').style.display = 'none';
}

function openDistrictTable(){
  console.log("open dist");
  document.getElementById('Comparison').style.display = 'none';
  document.getElementById('State').style.display = 'none';
  document.getElementById('District').style.display = 'block';
}

$('#pop-eq-input').on('keyup keydown change', function(e){
  var max = Number(document.getElementById('pop-eq-input').max);
  var min = Number(document.getElementById('pop-eq-input').min);
  console.log(max);
  console.log($(this).val() > max)
      if ($(this).val() > max 
          && e.keyCode !== 46
          && e.keyCode !== 8
         ) {
         e.preventDefault();     
         $(this).val(max);
      }

  console.log($(this).val() <min)
  if ($(this).val() < min 
      && e.keyCode !== 46
      && e.keyCode !== 8
      ) {
      e.preventDefault();     
      $(this).val(min);
  }
});

$('#graph-comp-input').on('keyup keydown change', function(e){
  var max = Number(document.getElementById('graph-comp-input').max);
  var min = Number(document.getElementById('graph-comp-input').min);
  console.log(max);
  console.log($(this).val() > max)
      if ($(this).val() > max 
          && e.keyCode !== 46
          && e.keyCode !== 8
         ) {
         e.preventDefault();     
         $(this).val(max);
      }

  console.log($(this).val() <min)
  if ($(this).val() < min 
      && e.keyCode !== 46
      && e.keyCode !== 8
      ) {
      e.preventDefault();     
      $(this).val(min);
  }
});

$('#rac-dev-input').on('keyup keydown change', function(e){
  var max = Number(document.getElementById('rac-dev-input').max);
  var min = Number(document.getElementById('rac-dev-input').min);
  console.log(max);
  console.log($(this).val() > max)
      if ($(this).val() > max 
          && e.keyCode !== 46
          && e.keyCode !== 8
         ) {
         e.preventDefault();     
         $(this).val(max);
      }

  console.log($(this).val() <min)
  if ($(this).val() < min 
      && e.keyCode !== 46
      && e.keyCode !== 8
      ) {
      e.preventDefault();     
      $(this).val(min);
  }
});

// $('#min-maj-input').on('keyup keydown change', function(e){
//   var max = Number(document.getElementById('min-maj-input').max);
//   var min = Number(document.getElementById('min-maj-input').min);
//   console.log(max);
//   console.log($(this).val() > max)
//       if ($(this).val() > max 
//           && e.keyCode !== 46
//           && e.keyCode !== 8
//          ) {
//          e.preventDefault();     
//          $(this).val(max);
//       }

//   console.log($(this).val() <min)
//   if ($(this).val() < min 
//       && e.keyCode !== 46
//       && e.keyCode !== 8
//       ) {
//       e.preventDefault();     
//       $(this).val(min);
//   }
// });

function setInputs(num){
  var values=document.getElementsByClassName('dist-1');
  var arr=[];
  for(var i=0;i<values.length;i++){ // get number out
    arr.push(values[i].innerText);
  }
  console.log(arr);
  document.getElementById('pop-eq-input').value=arr[num];
  document.getElementById('pop-eq-input').max=arr[num]-(-10);
  document.getElementById('pop-eq-input').min=arr[num]-10;
  
  document.getElementById('graph-comp-input').value=arr[num+1];
  document.getElementById('graph-comp-input').min=arr[num+1]-10;
  document.getElementById('graph-comp-input').max=arr[num+1]-(-10);
  document.getElementById('rac-dev-input').value=arr[num+2];
  document.getElementById('rac-dev-input').min=arr[num+2]-10;
  document.getElementById('rac-dev-input').max=arr[num+2]-(-10);
  // document.getElementById('min-maj-input').value=arr[num+3];
  // document.getElementById('min-maj-input').min=arr[num+3]-10;
  // document.getElementById('min-maj-input').max=arr[num+3]-(-10);
}

function openRed1(){
  console.log("openRed1 called");
  chosenPlan = '1';
  setInputs(1);
  document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
  
  if(currentState=="ga"){
    const off = ['ga_congressional_districts', 'ga_original_districts', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['ga_redistricting_1', 'ga_redistricting_1_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if (currentState=='pa'){
    const off = ['pa_congressional_districts', 'pa_original_districts', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['pa_redistricting_1', 'pa_redistricting_1_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if ( currentState=='nc'){
    const off = ['nc_congressional_districts', 'nc_original_districts', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['nc_redistricting_1', 'nc_redistricting_1_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else{
    chosenPlan = 'none';
    //error
  }
}

function openRed2(){
  console.log("openRed2 called");
  chosenPlan = '2';
  setInputs(6);
  document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
  if(currentState=="ga"){
    const off = ['ga_congressional_districts', 'ga_original_districts', 'ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_3', 'ga_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['ga_redistricting_2', 'ga_redistricting_2_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if (currentState=='pa'){
    const off = ['pa_congressional_districts', 'pa_original_districts', 'pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_3', 'pa_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['pa_redistricting_2', 'pa_redistricting_2_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if ( currentState=='nc'){
    const off = ['nc_congressional_districts', 'nc_original_districts', 'nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_3', 'nc_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['nc_redistricting_2', 'nc_redistricting_2_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else{
    chosenPlan = 'none';
    //error
  }
}

function openRed3(){
  console.log("openRed3 called");
  chosenPlan = '3';
  setInputs(11);
  document.getElementById('chosen-plan').innerText = "Generate a new plan using Plan " + chosenPlan;
  if(currentState=="ga"){
    const off = ['ga_congressional_districts', 'ga_original_districts', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_1', 'ga_redistricting_1_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['ga_redistricting_3', 'ga_redistricting_3_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if (currentState=='pa'){
    const off = ['pa_congressional_districts', 'pa_original_districts', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_1', 'pa_redistricting_1_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['pa_redistricting_3', 'pa_redistricting_3_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if ( currentState=='nc'){
    const off = ['nc_congressional_districts', 'nc_original_districts', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_1', 'nc_redistricting_1_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['nc_redistricting_3', 'nc_redistricting_3_map'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else{
    chosenPlan = 'none;'
    //error
  }
}

function fillComparisonTable() {
  var file;
  console.log(currentState);
  console.log(chosenPlan);
  console.log("in fill comparison table");
  if(currentState ==='ga'){
    file = "./data/georgia/ga-state-table-content.json";
  } else if(currentState === 'pa') {
    console.log("pa");
    file="./data/penn/pa-state-table-content.json";
  } else if(currentState === 'nc') {
    file="./data/north-carolina/nc-state-table-content.json";
  } else {
    console.log("error");
  }

  var newData =  {
    "number":"New Plan",
    "populationEquality":"93.76",
    "polsbyPopper":"8.13",
    "efficiencyGap":"83.79",
    "objFunction": "68.22"
  }

  var newArr = document.getElementsByClassName('new-red');
  var i=0;
  var j=0;
  var values = Object.values(newData);
  for(j=0;j<values.length;j++){
    newArr[i].innerText=values[j];
    i++;
  }

  var jsonData = {};
  $.getJSON(file, function(data){
    jsonData = data;
    var chosenArr = document.getElementsByClassName('chosen-red');
    i = 0;
    j = 0;
    if(chosenPlan === '1'){
      k = 0;
    } else if(chosenPlan === '2'){
      k = 1;
    } else {
      k = 2;
    }
    var values = Object.values(jsonData.district[k]);
    for(j=0;j<values.length;j++){
      chosenArr[i].innerText=values[j];
      i++;
    }

  });

}

function fillTable() {
  var file;
  console.log(currentState);
  console.log("in fill table");
  if(currentState=='ga'){
    file="./data/georgia/ga-state-table-content.json";
    document.getElementById('total-votes').innerText= "Total Votes: 4,998,482";
    document.getElementById('pop-total').innerText= "Total Population: 10,615,715";
    document.getElementById('state-table-header').innerText="Georgia State (2,679 Precincts)";
    document.getElementById('district-table-header').innerText="Georgia Redistrictings";
  } else if (currentState == 'pa'){
    console.log("pa");
    file="./data/penn/pa-state-table-content.json";
    document.getElementById('total-votes').innerText= "Total Votes: 6,835,903";
    document.getElementById('pop-total').innerText= "Total Population: 13,948,709";
    document.getElementById('state-table-header').innerText="Pennsylvania State (9,150 Precincts)";
    document.getElementById('district-table-header').innerText="Pennsylvania Redistrictings";
  }else if (currentState=='nc'){
    file="./data/north-carolina/nc-state-table-content.json";
    document.getElementById('total-votes').innerText= "Total Votes: 5,443,065";
    document.getElementById('pop-total').innerText= "Total Population: 11,334,913";
    document.getElementById('state-table-header').innerText="North Carolina State (2,704 Precincts)";
    document.getElementById('district-table-header').innerText="North Carolina Redistrictings";
  }else{
    //this should not happen
    console.log("error");
  }    
    var jsonData = {};
    $.getJSON(file,function(data){
        jsonData = data;
        var polArr=document.getElementsByClassName('pol-1');
        var i=0;
        var j=0;
        var k=0;
        for(k=0;k<2;k++){
          var values = Object.values(jsonData.political[k]);
          for(j=0;j<values.length;j++){
            polArr[i].innerText=values[j];
            i++;
          }
        }
        var polArr=document.getElementsByClassName('dem-1');
        i=0;
        for(k=0;k<5;k++){
          var values = Object.values(jsonData.demographic[k]);
          for(j=0;j<values.length;j++){
            polArr[i].innerText=values[j];
            i++;
          }
        }
        var polArr=document.getElementsByClassName('dist-1');
        i=0;
        for(k=0;k<3;k++){
          var values = Object.values(jsonData.district[k]);
          for(j=0;j<values.length;j++){
            polArr[i].innerText=values[j];
            i++;
          }
        }
    });
  

  
}

function openChosenPlan(){
  if(chosenPlan === '1'){
    openRed1();
  } else if(chosenPlan === '2'){
    openRed2();
  } else {
    openRed3();
  }
}

function openNewPlan(){
  if(currentState=="ga"){
    const off = ['ga_redistricting_1', 'ga_redistricting_1_map', 'ga_redistricting_2', 'ga_redistricting_2_map', 'ga_redistricting_3', 'ga_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['ga_congressional_districts', 'ga_original_districts'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if (currentState=='pa'){
    const off = ['pa_redistricting_1', 'pa_redistricting_1_map', 'pa_redistricting_2', 'pa_redistricting_2_map', 'pa_redistricting_3', 'pa_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['pa_congressional_districts', 'pa_original_districts'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else if ( currentState=='nc'){
    const off = ['nc_redistricting_1', 'nc_redistricting_1_map', 'nc_redistricting_2', 'nc_redistricting_2_map', 'nc_redistricting_3', 'nc_redistricting_3_map'];
    for (const id of off){
      map.setLayoutProperty(id, 'visibility', 'none');
    }
    const on = ['nc_congressional_districts', 'nc_original_districts'];
    for(const id of on){
      map.setLayoutProperty(id, 'visibility', 'visible');
    }
  }else{
    //error
  }
}
