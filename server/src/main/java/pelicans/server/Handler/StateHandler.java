package pelicans.server.Handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestPart;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import pelicans.server.Enum.States;

@Service
public class StateHandler {
    private States state;

    @Autowired
    public StateHandler() {
        this.state = null;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public String requestTableInfo(@PathParam("State") String state) {
        // modify path for your local machine
        String path = "Users/Alexa/CSE Work/CSE 416/416pelicans/server/main/data/";
        //String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        //States s = getState();
        switch (state){
            case "ga":
                path += "ga-state-table-content.json";
                break;
            case "nc":
                path += "nc-state-table-content.json";
                break;
            case "pa":
                path += "pa-state-table-content.geojson";
                break;
        } try {
            path = new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException ex){
            ex.printStackTrace();
        }
        return path;
    }
}