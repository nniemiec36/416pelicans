package pelicans.server.algo;

import pelicans.server.DistrictPlan;
import pelicans.server.Enum.States;
import org.locationtech.jts.geom.*;

import java.util.List;

public class Measures{

    private Algorithm algorithm;
    private DistrictPlan currentDistricting;
    private DistrictPlan enactedDistricting;

    public Algorithm getAlgorithm(){
        return algorithm;
    }

    public void setAlgorithm(Algorithm algorithm){
        this.algorithm = algorithm;
    }

    public DistrictPlan getCurrentDistricting() {
        return currentDistricting;
    }

    public void setCurrentDistricting(DistrictPlan currentDistricting) {
        this.currentDistricting = currentDistricting;
    }

    public DistrictPlan getEnactedDistricting() {
        return enactedDistricting;
    }

    public void setEnactedDistricting(DistrictPlan enactedDistricting) {
        this.enactedDistricting = enactedDistricting;
    }

    public List<Double> calculateMeasures(){return null;}
    public double calculateEfficiencyGap(){ return 0;}
    public double calculateEqualPopMeasure(){ return 0;}
    public double calculateSplitCountyMeasure(){ return 0;}
    public double calculatePolsbyPopScore(){ return 0;}
    public int calculateNumOppDistr(){ return 0;}
    public double calculateObjFuncScore(){ return 0;}
    public double calculateDeviation(States state){ return 0;}
    public double calculateDemPercent(){ return 0;}
    public String getHighestMinority(){ return null;}
    public void renumberPrecinctsAndDistricts(){ }
    // public BoxData getBoxData(Double d){ return null;}
    public boolean isDistrictPlanInteresting(){ return false;}
    public String generateSummary(){ return null;}
}