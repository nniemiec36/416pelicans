package pelicans.server.Enum;

public enum PopMeasure {
    TOTAL,
    VAP,
    CVAP
}