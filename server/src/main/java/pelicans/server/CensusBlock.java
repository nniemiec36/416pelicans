package pelicans.server;

import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.Precinct;
import pelicans.server.mapmodel.State;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;

@Entity
public class CensusBlock {
    @Id
    private String cb_id; // might need to rename this :(
    @Column(name = "block_id", nullable = false)
    private String blockID;
    private String geoID;
    private String stateAbbr;
    @ManyToOne
    private District district;
    @OneToMany
    private List<District> neighborDistricts;
    @ManyToOne
    private Precinct precincts;
    @ManyToOne
    private Population populationData;
    @OneToMany
    private List<CensusBlock> blockNeighbors;
    private Polygon geometry;
    private boolean isBorder;

    public CensusBlock(String blockID, String geoID, String stateAbbr, District district, List<District> neighborDistricts, Precinct precincts, Population populationData, List<CensusBlock> blockNeighbors, Polygon geometry) {
        this.blockID = blockID;
        this.geoID = geoID;
        this.stateAbbr = stateAbbr;
        this.district = district;
        this.neighborDistricts = neighborDistricts;
        this.precincts = precincts;
        this.populationData = populationData;
        this.blockNeighbors = blockNeighbors;
        this.geometry = geometry;
    }

    public String getBlockID() {
        return this.blockID;
    }

    public void setBlockID(String blockID) {
        this.blockID = blockID;
    }

    public String getGeoID() {
        return this.geoID;
    }

    public void setGeoID(String geoID) {
        this.geoID = geoID;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public District getDistrict() {
        return this.district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<District> getNeighborDistricts() {
        return this.neighborDistricts;
    }

    public void setNeighborDistricts(List<District> neighborDistricts) {
        this.neighborDistricts = neighborDistricts;
    }

    public Precinct getPrecincts() { return this.precincts; }

    public void setPrecincts(Precinct precincts) {
        this.precincts = precincts;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public List<CensusBlock> getBlockNeighbors() {
        return this.blockNeighbors;
    }

    public void setBlockNeighbors(List<CensusBlock> blockNeighbors) {
        this.blockNeighbors = blockNeighbors;
    }

    public Polygon getGeometry() {
        return this.geometry;
    }

    public String getCb_id() {
        return cb_id;
    }

    public void setCb_id(String cb_id) {
        this.cb_id = cb_id;
    }

    public boolean isBorder() {
        return isBorder;
    }

    public void setBorder(boolean border) {
        isBorder = border;
    }

    public District getNeighborDistrict(){
        return null;
    }

    public void setGeometry(Polygon geometry) {
        this.geometry = geometry;
    }

    public int hashCode(){
        return 0;
    }
}