package pelicans.server;

import org.locationtech.jts.geom.*;
import java.util.List;

import org.json.simple.JSONObject;
import pelicans.server.Enum.Demographic;
import pelicans.server.Enum.Party;
import pelicans.server.Enum.States;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.State;

import javax.persistence.*;

@Entity
public class DistrictPlan {
    private String stateAbbr;
    @Id
    @GeneratedValue
    private String id;
    @OneToMany
    private List<District> districts;
    private double objFunctScore;
    private double efficiencyGap;
    private double equalPopMeasure;
    private double polsbyPopScore;
    private int numOppDistricts;
    @OneToMany
    private List<Population> populationData;
    @ManyToOne
    private BoxData boxData;
    private boolean isInteresting;

    public DistrictPlan(String stateAbbr, String id, List<District> districts, double objFunctScore, double efficiencyGap, double equalPopMeasure, double polsbyPopScore, int numOppDistricts, List<Population> populationData, BoxData boxData, boolean isInteresting) {
        this.stateAbbr = stateAbbr;
        this.id = id;
        this.districts = districts;
        this.objFunctScore = objFunctScore;
        this.efficiencyGap = efficiencyGap;
        this.equalPopMeasure = equalPopMeasure;
        this.polsbyPopScore = polsbyPopScore;
        this.numOppDistricts = numOppDistricts;
        this.populationData = populationData;
        this.boxData = boxData;
        this.isInteresting = isInteresting;
        //this.districtingBoundaries = districtingBoundaries;
        //this.splitCountyMeasure = splitCountyMeasure;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<Polygon> getDistrictingBoundaries() {
        List<Polygon> boundaries = null;
        for(int i = 0; i < districts.size(); i++){
            boundaries.add(districts.get(i).getBoundaryData());
        }
        return boundaries;
    }

    public double getObjFunctScore() {
        return objFunctScore;
    }

    public void setObjFunctScore(double objFunctScore) {
        this.objFunctScore = objFunctScore;
    }

    public double getEfficiencyGap() {
        return efficiencyGap;
    }

    public void setEfficiencyGap(double efficiencyGap) {
        this.efficiencyGap = efficiencyGap;
    }

    public double getEqualPopMeasure() {
        return equalPopMeasure;
    }

    public void setEqualPopMeasure(double equalPopMeasure) {
        this.equalPopMeasure = equalPopMeasure;
    }

    public double getPolsbyPopScore() {
        return polsbyPopScore;
    }

    public void setPolsbyPopScore(double polsbyPopScore) {
        this.polsbyPopScore = polsbyPopScore;
    }

    public int getNumOppDistricts() {
        return numOppDistricts;
    }

    public void setNumOppDistricts(int numOppDistricts) {
        this.numOppDistricts = numOppDistricts;
    }

    public List<Population> getPopulationData() {
        return populationData;
    }

    public void setPopulationData(List<Population> populationData) {
        this.populationData = populationData;
    }

    public BoxData getBoxData() {
        return boxData;
    }

    public void setBoxData(BoxData boxData) {
        this.boxData = boxData;
    }

    public boolean isInteresting() {
        return isInteresting;
    }

    public void setInteresting(boolean interesting) {
        isInteresting = interesting;
    }

    public JSONObject generateSummary(){
        JSONObject planSummary = new JSONObject();
        planSummary.put("id", this.id);
        planSummary.put("population data", this.populationData.toString());
        planSummary.put("polsby popper", this.polsbyPopScore);
        planSummary.put("population equality", this.equalPopMeasure);
        planSummary.put("obj function", this.objFunctScore);
        return planSummary;
    }

    public JSONObject constructClientJSON(){
        return null;
    }

    public JSONObject calculateMeasures(){
        //District distr = new District();
        District distr = this.districts.get(0);
        Double objScore = distr.getObjFunctScore();
        Double polsScore = distr.getPolsbyPopScore();
        JSONObject json = new JSONObject();
        json.put("Objective Score", objScore);
        json.put("Polsby Popper Score", polsScore);
        return json;
    }
    
    public District getDistrictByID(int id){
        return null;
    }

    public Population[] getUpdatedPopulations() {
        return null;
    }
}

// old code
/*
public void setDistrictingBoundaries(List<Polygon> districtingBoundaries) {
    this.districtingBoundaries = districtingBoundaries;
}
*/