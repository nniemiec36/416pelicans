package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.mapmodel.Precinct;

@Repository
public interface PrecinctRepository extends CrudRepository<Precinct, String> {
    <T>T findByPrecinctIDAndStateAbbr(String precinctID, String stateAbbr, Class<T> tClass);
}