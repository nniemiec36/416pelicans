package pelicans.server.respository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pelicans.server.BoxData;

@Repository
public interface BoxDataRepository extends CrudRepository<BoxData, String> {
    <T>T findByBoxId(String boxId, Class<T> tClass);
}

