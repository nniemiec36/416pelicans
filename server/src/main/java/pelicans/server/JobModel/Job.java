package pelicans.server.JobModel;

import pelicans.server.DistrictPlan;
import pelicans.server.Enum.States;
import pelicans.server.*;

import java.util.List;
import java.util.ArrayList;

public class Job{
    private States state;
    private int jobId;
    private String jobStatus;
    private DistrictPlan districtPlan;

    public Job(States state, int jobId, String jobStatus, DistrictPlan districtPlan) {
        this.state = state;
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.districtPlan = districtPlan;
    }

    public States getState() {
        return state;
    }

    public void setState(States state) {
        this.state = state;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public DistrictPlan getDistrictPlan() {
        return districtPlan;
    }

    public void setDistrictPlan(DistrictPlan districtPlan) {
        this.districtPlan = districtPlan;
    }

    public String generateJobSummary(){ return null;}
}