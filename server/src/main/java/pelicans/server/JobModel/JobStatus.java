package pelicans.server.JobModel;

public enum JobStatus {
    PENDING,
    QUEUED,
    RUNNING,
    COMPLETED,
    CANCELLED,
    ERROR
}