package pelicans.server.mapmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.simple.JSONObject;
import pelicans.server.CensusBlock;
import pelicans.server.Enum.States;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;
import java.util.Set;

@Entity
public class District{
    private String stateAbbr;
    private int counties;
    @Id
    @GeneratedValue
    private int districtNum;
    private String geoId;
    private String districtPlanId;
    //@ManyToOne
    //@JoinColumn(name = "population_data_pop_id")
    @ManyToOne
    private Population populationData;
    @Transient
    private Set<String> countyNames;
    @OneToMany
    private List<Precinct> precincts;
    @ManyToMany
    private List<District> districtNeighbors;
    private Polygon boundaryData;
    private double polsbyPopScore;
    private double objFunctScore;
    private double equalPopMeasure;
    private double efficiencyGap;
    @OneToMany
    private List<CensusBlock> censusBlocks;

    public District(String stateAbbr, int counties, int districtNum, String geoId, String districtPlanID, Population populationData, Set<String> countyNames, List<Precinct> precincts, List<District> districtNeighbors, Polygon boundaryData) {
        this.stateAbbr = stateAbbr;
        this.counties = counties;
        this.districtNum = districtNum;
        this.geoId = geoId;
        this.districtPlanId = districtPlanID;
        this.populationData = populationData;
        this.countyNames = countyNames;
        this.precincts = precincts;
        this.districtNeighbors = districtNeighbors;
        this.boundaryData = boundaryData;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public int getCounties() {
        return counties;
    }

    public void setCounties(int counties) {
        this.counties = counties;
    }

    public int getDistrictNum() {
        return districtNum;
    }

    public void setDistrictNum(int districtNum) {
        this.districtNum = districtNum;
    }

    public String getGeoId() {
        return geoId;
    }

    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    public String getDistrictPlanId() {
        return districtPlanId;
    }

    public void setDistrictPlanId(String districtPlanId) {
        this.districtPlanId = districtPlanId;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public Set<String> getCountyNames() {
        return countyNames;
    }

    public void setCountyNames(Set<String> countyNames) {
        this.countyNames = countyNames;
    }

    public List<Precinct> getPrecincts() {
        return precincts;
    }

    public void setPrecincts(List<Precinct> precincts) {
        this.precincts = precincts;
    }

    public List<District> getDistrictNeighbors() {
        return districtNeighbors;
    }

    public void setDistrictNeighbors(List<District> districtNeighbors) {
        this.districtNeighbors = districtNeighbors;
    }

    public Polygon getBoundaryData() {
        return boundaryData;
    }

    public void setBoundaryData(Polygon boundaryData) {
        this.boundaryData = boundaryData;
    }

    public List<Polygon> getPrecinctsBoundaryData() {
        List<Polygon> boundaries = null;
        for(int i = 0; i < precincts.size(); i++){
            boundaries.add(precincts.get(i).getBoundaryData());
        }
        return boundaries;
    }

    public List<Polygon> getCensusBlockBoundaryData() {
        List<Polygon> boundaries = null;
        for(int i = 0; i < censusBlocks.size(); i++){
           boundaries.add(censusBlocks.get(i).getGeometry());
        }
       return boundaries;
    }

    public boolean isOpportunityDistrict(Population populationData){
        return false;
    }

    public double getOpportunityPercentage(Population populationData){
        return 0.0;
    }

    public int compareTo(District d){
        return 0;
    }

    public JSONObject districtSummary(){
        return null;
    }

    public List<Polygon> getPrecinctBoundaries(){
        return null;
    }

    public void addCensusBlock(CensusBlock c){
        this.censusBlocks.add(c);
    }

    public void removeCensusBlock(CensusBlock c){
        this.censusBlocks.remove(c);
    }

    public CensusBlock getCensusBlock(){
        return null;
    }

    public JSONObject generateSummary() {
        return null;
    }

    public double getPolsbyPopScore() {
        return this.polsbyPopScore;
    }

    public void setPolsbyPopScore(double polsbyPopScore) {
        this.polsbyPopScore = polsbyPopScore;
    }

    public double getEfficiencyGap() {
        return this.efficiencyGap;
    }

    public void setEfficiencyGap(double efficiencyGap) {
        this.efficiencyGap = efficiencyGap;
    }

    public double getEqualPopMeasure() {
        return equalPopMeasure;
    }

    public void setEqualPopMeasure(double equalPopMeasure) {
        this.equalPopMeasure = equalPopMeasure;
    }
    
    public double getObjFunctScore() {
        return objFunctScore;
    }

    public void setObjFunctScore(double objFunctScore) {
        this.objFunctScore = objFunctScore;
    }
}