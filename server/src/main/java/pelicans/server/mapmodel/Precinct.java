package pelicans.server.mapmodel;

import pelicans.server.CensusBlock;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;
import java.util.List;

@Entity
public class Precinct {
    @Id
    @GeneratedValue
    private String precinctID;
    private String geoID;
    private String stateAbbr;
    @ManyToOne
    private Population populationData;
    @OneToMany
    //@JoinColumn(name = "census_block_id")
    private List<CensusBlock> censusBlocks;
    private Polygon boundaryData;
    @ManyToOne
    private District district;
    @ElementCollection
    private List<Integer> precinctNeighborIds;
    //@OneToMany
    //private List<CensusBlock> censusNeighbors;

    public Precinct(String precinctID, String geoID, String stateAbbr, Population populationData, List<CensusBlock> censusBlocks, Polygon boundaryData, District district, List<Integer> precinctNeighborIds) {
        this.precinctID = precinctID;
        this.geoID = geoID;
        this.stateAbbr = stateAbbr;
        this.populationData = populationData;
        this.censusBlocks = censusBlocks;
        this.boundaryData = boundaryData;
        this.district = district;
        this.precinctNeighborIds = precinctNeighborIds;
    }

    public String getPrecinctID() {
        return precinctID;
    }

    public void setPrecinctID(String precinctID) {
        this.precinctID = precinctID;
    }

    public String getGeoID() {
        return geoID;
    }

    public void setGeoID(String geoID) {
        this.geoID = geoID;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public List<CensusBlock> getCensusBlocks() {
        return censusBlocks;
    }

    public void setCensusBlocks(List<CensusBlock> censusBlocks) {
        this.censusBlocks = censusBlocks;
    }

    public Polygon getBoundaryData() {
        return boundaryData;
    }

    public void setBoundaryData(Polygon boundaryData) {
        this.boundaryData = boundaryData;
    }

    public District getDistrict() {
        return this.district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public List<Integer> getPrecinctNeighborIds() {
        return this.precinctNeighborIds;
    }

    public void setPrecinctNeighbors(List<Integer> precinctNeighborIds) {
        this.precinctNeighborIds = precinctNeighborIds;
    }

    public String getOriginalDistrictID(){return null;}
    public String toString(){return null;}
    public void addCensusBlock(CensusBlock c){ }
    public void removeCensusBlock(CensusBlock c){ }
    public CensusBlock getCensusBlock(){ return null;}
    public List<Polygon> getCensusBlockBoundaries(){return null;}
}

// old code 
/*
   public List<CensusBlock> getCensusNeighbors() {
       return censusNeighbors;
   }

   public void setCensusNeighbors(List<CensusBlock> censusNeighbors) {
       this.censusNeighbors = censusNeighbors;
   }
 */