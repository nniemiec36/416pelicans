package pelicans.server.mapmodel;

import pelicans.server.*;
import pelicans.server.Enum.*;
import java.util.List;

//import javafx.util.Pair;
import org.json.simple.JSONObject;
import pelicans.server.BoxData;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.BoxMeasure;
import pelicans.server.Enum.PopMeasure;
import pelicans.server.Enum.States;
import pelicans.server.Population;

import javax.persistence.*;
import org.locationtech.jts.geom.*;

@Entity
public class State {
    private String stateAbbr;
    @Id
    @GeneratedValue
    private String id;
    @OneToMany
    private List<District> enactedDistricts;
    @ManyToOne
    private DistrictPlan enactedDistrictPlan;
    @ManyToOne
    private DistrictPlan updatedDistrictPlan;
    @ManyToOne
    @JoinColumn(name="population_data_pop_id")
    private Population populationData;
    private PopMeasure popVariable;
    private Polygon boundaryData;
    @OneToMany
    @JoinColumns({
            @JoinColumn(name="stateAbbr", referencedColumnName="stateAbbr")
    })
    private List<DistrictPlan> seaWulfPlans;
    @OneToMany
    private List<BoxData> boxData;
    private JSONObject seaWulfPlansSummary;

    public State(String stateAbbr){
        this.stateAbbr = stateAbbr;
    }

    public State(String stateAbbr, String id, List<District> enactedDistricts, DistrictPlan enactedDistrictPlan, DistrictPlan updatedDistrictPlan, Population populationData, PopMeasure popVariable, Polygon boundaryData, List<DistrictPlan> seaWulfPlans, List<BoxData> boxData, JSONObject seaWulfPlansSummary) {
        this.stateAbbr = stateAbbr;
        this.id = id;
        this.enactedDistricts = enactedDistricts;
        this.enactedDistrictPlan = enactedDistrictPlan;
        this.updatedDistrictPlan = updatedDistrictPlan;
        this.populationData = populationData;
        this.popVariable = popVariable;
        this.boundaryData = boundaryData;
        this.seaWulfPlans = seaWulfPlans;
        this.boxData = boxData;
        this.seaWulfPlansSummary = seaWulfPlansSummary;
    }

    public String getStateAbbr() {
        return this.stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<District> getEnactedDistricts() {
        return enactedDistricts;
    }

    public void setEnactedDistricts(List<District> enactedDistricts) {
        this.enactedDistricts = enactedDistricts;
    }

    public DistrictPlan getEnactedDistrictPlan() {
        return enactedDistrictPlan;
    }

    public void setEnactedDistrictPlan(DistrictPlan enactedDistrictPlan) {
        this.enactedDistrictPlan = enactedDistrictPlan;
    }

    public DistrictPlan getUpdatedDistrictPlan() {
        return updatedDistrictPlan;
    }

    public void setUpdatedDistrictPlan(DistrictPlan updatedDistrictPlan) {
        this.updatedDistrictPlan = updatedDistrictPlan;
    }

    public Population getPopulationData() {
        String pop = populationData.formatForTable();       // returns a string, but return type for this method is Population
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public PopMeasure getPopVariable() {
        return popVariable;
    }

    public void setPopVariable(PopMeasure popVariable) {
        this.popVariable = popVariable;
    }

    public Polygon getBoundaryData() {
        return boundaryData;
    }

    public void setBoundaryData(Polygon boundaryData) {
        this.boundaryData = boundaryData;
    }

    public List<DistrictPlan> getSeaWulfPlans() {
        return seaWulfPlans;
    }

    public void setSeaWulfPlans(List<DistrictPlan> seaWulfPlans) {
        this.seaWulfPlans = seaWulfPlans;
    }

    public List<BoxData> getBoxData() {
        return boxData;
    }

    public void setBoxData(List<BoxData> boxData) {
        this.boxData = boxData;
    }

    public JSONObject getSeaWulfPlansSummary() {
        return seaWulfPlansSummary;
    }

    public void setSeaWulfPlansSummary(JSONObject seaWulfPlansSummary) {
        this.seaWulfPlansSummary = seaWulfPlansSummary;
    }

    public JSONObject getRedistrictingBoundary(int id){ // return type was a String --> JSONObject
        JSONObject json = new JSONObject();
        json.put(id, enactedDistrictPlan.getDistrictingBoundaries());
        return json;
    }

    public District getDistrictByID(int id){
        return null;
    }

    public String getDistrictingSummary(int id){
        return null;
    }

    public JSONObject getPlan(String id){
        int planID = Integer.parseInt(id);
        DistrictPlan plan = seaWulfPlans.get(planID);
        return plan.generateSummary();
    }

    public void addToBoxDataArray(BoxData data){

    }

    public DistrictPlan getRedistrictingByNumber(int number){
        return null;
    }

    public List<Polygon> getDistrictBoundaries(){
        return null;
    }

    public List<List<Polygon>> getAllBoundaries(){
        return null;
    }

    public JSONObject generateBoxAndWhiskerPlot(List<DistrictPlan> plan, BoxMeasure compVal){
        // Enity manager from GUI 10 --> box and whisker
        return null;
    }

    public JSONObject getCurrentAndUpdated(int id){
        return null;
    }
}

// old code
/*
   public Pair<DistrictPlan, DistrictPlan> getCurrentAndUpdated(int id){
       return null;
   }
*/