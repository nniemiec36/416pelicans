package pelicans.server.dataAccess;

public class SeaWulfProperties {

    private String netID;
    private String password;
    private String bashScript;
    private String bashScriptPath;
    private String slurmScript;
    private String slurmScriptPath;
    private String generateSummaryScript;
    private String districtingThreshold;
    private String summaryFileName;
    private String jobScript;

    public SeaWulfProperties(String netID, String password, String bashScript, String bashScriptPath, String slurmScript, String slurmScriptPath, String generateSummaryScript, String districtingThreshold, String summaryFileName, String jobScript) {
        this.netID = netID;
        this.password = password;
        this.bashScript = bashScript;
        this.bashScriptPath = bashScriptPath;
        this.slurmScript = slurmScript;
        this.slurmScriptPath = slurmScriptPath;
        this.generateSummaryScript = generateSummaryScript;
        this.districtingThreshold = districtingThreshold;
        this.summaryFileName = summaryFileName;
        this.jobScript = jobScript;
    }

    public String getNetID() {
        return netID;
    }

    public void setNetID(String netID) {
        this.netID = netID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBashScript() {
        return bashScript;
    }

    public void setBashScript(String bashScript) {
        this.bashScript = bashScript;
    }

    public String getBashScriptPath() {
        return bashScriptPath;
    }

    public void setBashScriptPath(String bashScriptPath) {
        this.bashScriptPath = bashScriptPath;
    }

    public String getSlurmScript() {
        return slurmScript;
    }

    public void setSlurmScript(String slurmScript) {
        this.slurmScript = slurmScript;
    }

    public String getSlurmScriptPath() {
        return slurmScriptPath;
    }

    public void setSlurmScriptPath(String slurmScriptPath) {
        this.slurmScriptPath = slurmScriptPath;
    }

    public String getGenerateSummaryScript() {
        return generateSummaryScript;
    }

    public void setGenerateSummaryScript(String generateSummaryScript) {
        this.generateSummaryScript = generateSummaryScript;
    }

    public String getDistrictingThreshold() {
        return districtingThreshold;
    }

    public void setDistrictingThreshold(String districtingThreshold) {
        this.districtingThreshold = districtingThreshold;
    }

    public String getSummaryFileName() {
        return summaryFileName;
    }

    public void setSummaryFileName(String summaryFileName) {
        this.summaryFileName = summaryFileName;
    }

    public String getJobScript() {
        return jobScript;
    }

    public void setJobScript(String jobScript) {
        this.jobScript = jobScript;
    }

    public void getProperties(){
    }
}