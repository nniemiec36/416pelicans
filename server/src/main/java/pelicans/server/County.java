package pelicans.server;

import pelicans.server.Enum.States;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.Precinct;

import java.util.List;
import org.locationtech.jts.geom.*;

public class County {
    private String name;
    private String countyID;
    private String geoID;
    private String stateAbbr;
    private List<District> districts;
    private List<CensusBlock> censusBlocks;
    private List<Precinct> precincts;
    private Population populationData;
    private Polygon geometry;

    public County(String name, String countyID, String geoID, String stateAbbr, List<District> districts, List<CensusBlock> censusBlocks, List<Precinct> precincts, Population populationData, Polygon geometry) {
        this.name = name;
        this.countyID = countyID;
        this.geoID = geoID;
        this.stateAbbr = stateAbbr;
        this.districts = districts;
        this.censusBlocks = censusBlocks;
        this.precincts = precincts;
        this.populationData = populationData;
        this.geometry = geometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountyID() {
        return countyID;
    }

    public void setCountyID(String countyID) {
        this.countyID = countyID;
    }

    public String getGeoID() {
        return geoID;
    }

    public void setGeoID(String geoID) {
        this.geoID = geoID;
    }

    public String getStateAbbr() {
        return stateAbbr;
    }

    public void setStateAbbr(String stateAbbr) {
        this.stateAbbr = stateAbbr;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public List<CensusBlock> getCensusBlocks() {
        return censusBlocks;
    }

    public void setCensusBlocks(List<CensusBlock> censusBlocks) {
        this.censusBlocks = censusBlocks;
    }

    public List<Precinct> getPrecincts() {
        return precincts;
    }

    public void setPrecincts(List<Precinct> precincts) {
        this.precincts = precincts;
    }

    public Population getPopulationData() {
        return populationData;
    }

    public void setPopulationData(Population populationData) {
        this.populationData = populationData;
    }

    public Polygon getGeometry() {
        return geometry;
    }

    public void setGeometry(Polygon geometry) {
        this.geometry = geometry;
    }
}