package pelicans.server.projections;

import pelicans.server.DistrictPlan;
import pelicans.server.Population;

import java.util.List;

public interface StateProj {
    String getId();
    Population getPopulationData();
}

