package pelicans.server.api;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import pelicans.server.BoxData;
import pelicans.server.DistrictPlan;
import pelicans.server.Enum.States;
import pelicans.server.JobModel.*;
import java.util.Objects.*;

public class JobController{
    private List<States> statesList;
    private States currentState;
    private DistrictPlan currentPlan;

    public JobController(List<States> statesList, States currentState, DistrictPlan currentPlan) {
        this.statesList = statesList;
        this.currentState = currentState;
        this.currentPlan = currentPlan;
    }

    public List<States> getStatesList() {
        return statesList;
    }

    public void setStatesList(List<States> statesList) {
        this.statesList = statesList;
    }

    public States getCurrentState() {
        return currentState;
    }

    public void setCurrentState(States currentState) {
        this.currentState = currentState;
    }

    public DistrictPlan getCurrentPlan() {
        return currentPlan;
    }

    public void setCurrentPlan(DistrictPlan currentPlan) {
        this.currentPlan = currentPlan;
    }

    public List<Job> handleCreateJob(Job job){
        return null;
    }

    public List<Job> handleCancelJob(Job job){
        return null;
    }

    public List<Job> handleDeleteJob(Job job){
        return null;
    }

    public List<Job> getJobHistory(Job job){
        return null;
    }

    public List<Job> getSummaryFile(Job job){
        return null;
    }

    public boolean sendResultsToClient(){
        return false;
    }

    public List<BoxData> generateBoxPlot(){
        return null;
    }

    public List<Job> getResults(){
        return null;
    }
}