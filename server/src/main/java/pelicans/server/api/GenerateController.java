package pelicans.server.api;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import pelicans.server.DistrictPlan;
import pelicans.server.Enum.RunningState;
import pelicans.server.algo.Algorithm;
import pelicans.server.algo.Generate;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.State;

import javax.ws.rs.Path;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/generate")

public class GenerateController {
//
//    private Generate currentGen;
//    private State currentState;
//    private District distr;
//    private Algorithm algo;

//    public boolean isUserThresholdValid(){
//        JSONObject summary = algo.getCurrentBriefSummary();
//        if(algo.getCurrentIteration() > algo.getMaxIteration()) {
//                // && algo.calculateQualityMove() == false){
//            return false;
//        }
//        return true;
//    }
//
//    public List<DistrictPlan> getComparedPlans(){
//        return null;
//    }
//
//    public JSONObject getUpdatedPopulations(){
//        return null;
//    }
//
//     @PostMapping("/state/{id}")
//     public JSONObject runGenerate(@PathVariable("id") int id){
//
//        //create a new generate object
//         this.currentGen = new Generate();
//         currentGen.setIsRunning(RunningState.RUNNING);
//         currentGen.startAlgorithm();
//         return null;
//     }
//
//    @GetMapping("/getProgress/")
//    @ResponseBody
//    public JSONObject getAlgProgress(){
//        return currentGen.getAlgorithmSummary();
//    }
//
//    @GetMapping("/getGraphicProgress")
//    @ResponseBody
//    public JSONObject getAlgGraphicProgress(){
//        JSONObject json = new JSONObject();
//        json.put("Population", currentGen.getUpdatedPopulations());
//        return json;
//    }
//
//    @PostMapping("/{userThresholds}")
//    public String setAlgParameters(@PathVariable("userThresholds") String params){
//        return currentGen.setAlgParams(params);
//    }

//    @GetMapping("/checkParameters/{timeLimit}/{popEquality}/{compactness}/{minMaj}")
//    @ResponseBody
//    public String checkParameters(@PathVariable("timeLimit") int timeLimitSecs,
//                                  @PathVariable("popEquality") double popEquality,
//                                  @PathVariable("compactness") double compactness,
//                                  @PathVariable("minMaj") int minMajDists) throws Exception{
//        System.out.println("timeLimit: " + timeLimitSecs);
//        System.out.println("popEquality: " + popEquality);
//        System.out.println("compactness: " + compactness);
//        System.out.println("minMaj: " + minMajDists);
//        if(!validateLimits(timeLimitSecs, popEquality, compactness, minMajDists))
//            throw new Exception("error");
//        else {
//            currentGen.setCompactnessThres(compactness);
//            currentGen.setMinMajDistrThres(minMajDists);
//            currentGen.setTimeOutLimit(timeLimitSecs);
//            currentGen.setPopEqualityThres(popEquality);
//        }
//        return "welp it worked";
//    }
    
//    @GetMapping("/getResults")
//    @ResponseBody
//    public JSONObject getResults(){
//        JSONObject json = new JSONObject();
//        DistrictPlan plan = currentState.getUpdatedDistrictPlan();
//        Algorithm alg = currentGen.getAlgorithm();
//        JSONObject summary = distr.generateSummary();   // for server 2
//        json.put("Updated District Plan", plan);
//        json.put("Algorithm", alg);
//        json.put("Summary", summary);
//        return json;
//    }
//    @GetMapping("/stop")
//    public JSONObject stopGenerate(){
//        return currentGen.stopAlgorithm();
//    }
//
//    @PostMapping("/sendTest")
//    public String sendTest(@Validated @RequestBody String ex){
//        System.out.println(ex);
//        return "This is a post test!";
//    }

//    @GetMapping("/test")
//    @ResponseBody
//    public String testing(){
//        System.out.println("this is a test!!");
//        return "this is a test!!";
//    }
//
//    @PostMapping("/putObjTest/{var}")
//    public @ResponseBody void testSetObject(@PathVariable("var") String var){
//        System.out.println("var");
//        this.currentGen = new Generate();
//        System.out.println("Set currentGen to: " + this.currentGen);
//    }
//
//    @GetMapping("/getObjTest")
//    @ResponseBody
//    public String getSetObject(){
//        System.out.println("Get currentGen value: " + this.currentGen);
//        return "success";
//    }
//
//    public boolean validateLimits(int timeSeconds, double popEquality, double compactness, double minMaj){
//        if(timeSeconds <= 0)
//            return false;
//        if(popEquality < 0 || popEquality > 1.0)
//            return false;
//        if(compactness < 0 || compactness > 1.0)
//            return false;
//
//        // int distNum = currentState.getNumDistricts()
//        // gonna hard code it to 12 for now
//        if(minMaj > 12)
//            return false;
//
//        return true;
//    }

}
