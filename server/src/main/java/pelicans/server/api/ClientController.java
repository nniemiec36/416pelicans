package pelicans.server.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pelicans.server.DistrictPlan;
import pelicans.server.Population;
import pelicans.server.Enum.BoxMeasure;
import pelicans.server.Enum.PopMeasure;
import pelicans.server.Enum.States;
import pelicans.server.mapmodel.District;
import pelicans.server.mapmodel.State;
import org.json.simple.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/client")

public class ClientController {
    private List<States> statesList;
    private PopMeasure popVar;
    private State currentState;
    private DistrictPlan currentPlan;
    private District district;

//    public void refresh(){}
//
//    public void changeState(String s){}
//
//    public JSONObject getStateBoundaries(String state){return null;}
//
//    public JSONObject getTableData(){
//        return null;
//    }
//
//    public String getAlgoParameters(String s){
//        return null;
//    }
//
//    @PostMapping("/populationVariable/set")
//    public void setPopVariable(@Validated @RequestBody JSONObject pop){
//        System.out.println(pop.toString());
//        PopMeasure var;
//        String[] temp = pop.toJSONString().split("\\:");
//        String val = temp[1];
//        val = val.substring(1, val.length()-2);
//        System.out.println(val);
//
//        // String popVar = pop.get("title");
//        // pop.getJSONObject("title");
//        // System.out.println("popVar: " + popVar);
//
//        if(val.equals("total")){
//            var = PopMeasure.TOTAL;
//            System.out.println("TRUE!!");
//        }
//        else{
//            var = PopMeasure.VAP;
//        }
//        System.out.println("pop....: " + pop);
//        System.out.println("VAR is:");
//        this.popVar = var;
//        // setPopVariable(var);
//    }

    @GetMapping("getDistrictTableInfo/{state}/{populationVar}")
    @ResponseBody
    public String getDistrictTableInfo(@PathVariable("state") String state,
                                        @PathVariable("populationVar") String pop){
        // String path = "/Users/Alexa/Desktop/CSE Work/CSE 416/416pelicans/server/src/main/data/";
        String path = "/Users/nicoleniemiec/desktop/416/416pelicans/server/src/main/data/";
        // String path = "/Users/nitindev/Documents/GitHub/416Gitlab/416pelicans/server/src/main/data/";
        // String path = System.getProperty("java.class.path").split("server")[0] + "server/src/main/data/";
        // States s = getState();
        System.out.println(state);
        System.out.print(pop);
        path += state + "-cd-" + pop + "-pop.json";
        try {
            path = new String(Files.readAllBytes(Paths.get(path)));
            System.out.println(path);
        } catch (IOException ex){
            ex.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "error reading file", ex);
        }
        return path;
    }
        
    public JSONObject mergeJSONs(JSONObject first, JSONObject second){
        return null;
    }

    @GetMapping("/seawulfPlans/{id}")
    @ResponseBody
    public JSONObject getPlanData(@PathVariable("id") String id){
        return currentState.getPlan(id);
    }
    @GetMapping("/minMajStatus/")
    @ResponseBody
    public JSONObject getMinMajStats(@PathVariable() DistrictPlan currentPlan){
        JSONObject json = new JSONObject();
        Population[] pop = currentPlan.getUpdatedPopulations();
        json.put("TOTAL",pop[0]);
        json.put("VAP",pop[1]);
        return json;
    }

    @GetMapping("/comparePlans/{id}")
    @ResponseBody   
    public JSONObject comparePlans(@PathVariable("id")String id){
        JSONObject json = new JSONObject();
        int planId = Integer.parseInt(id);
        json.put(currentState.getCurrentAndUpdated(planId), currentState.getCurrentAndUpdated(planId)); // getkey --> 0th index, getvalue --> 1st index
        // getKey() and getval() ??
        return json;
    }

    @GetMapping("/boxAndWhisker/{plan}/{comparisonBasis}")
    @ResponseBody
    public JSONObject getBoxAndWhiskerPlot(@PathVariable() List<DistrictPlan> plans, BoxMeasure comparisonVar){
        return currentState.generateBoxAndWhiskerPlot(plans, comparisonVar);
    }

    @GetMapping("/stateBoundariess/{state}")
    @ResponseBody
    public String getState(){
        // entity manager? --> gui 2
        return null;
    }

    @GetMapping("/redistricting/{planID}")
    @ResponseBody
    public JSONObject getRedistricting(@PathVariable() int id ){
        return currentState.getRedistrictingBoundary(id);
    }
 
    @GetMapping("/redistricting/{planID}/{distrID}")
    @ResponseBody
    public String getDistInRedist(@PathVariable() int number, int id){
        DistrictPlan redistricting = currentState.getRedistrictingByNumber(number);
        District dist = currentPlan.getDistrictByID(id);
        JSONObject json = district.districtSummary();
        return json.toString();
    }
}
